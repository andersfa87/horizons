Horizons.Util = {}

--[[ 
	Creates a standard clickable player link
]]
function Horizons.Util.CreateCharacterLink(name)
	return "\124r\124Hplayer:"..name.."\124h["..name.."]\124h"
end



--[[

]]
function Horizons.Util:RGBtoHEX(r, g, b)
	r = r <= 1 and r >= 0 and r or 0
	g = g <= 1 and g >= 0 and g or 0
	b = b <= 1 and b >= 0 and b or 0
	return string.format("%02x%02x%02x", r*255, g*255, b*255)
end

function Horizons.Util:GetItemSlotMap()
	local slotMap = {   
				["INVTYPE_HEAD"] = 1,
				["INVTYPE_NECK"] = 2,
				["INVTYPE_SHOULDER"] = 3,
				["INVTYPE_BODY"] = 4,
				["INVTYPE_ROBE"] = 5,
				["INVTYPE_CHEST"] = 5,
				["INVTYPE_WAIST"] = 6,
				["INVTYPE_LEGS"] = 7,
				["INVTYPE_FEET"] = 8,
				["INVTYPE_WRIST"] = 9,
				["INVTYPE_HAND"] = 10,
				["INVTYPE_FINGER"] = 11,
				["INVTYPE_TRINKET"] = 13,
				["INVTYPE_CLOAK"] = 15,
				["INVTYPE_WEAPON"] = 16,
				["INVTYPE_WEAPONMAINHAND"] = 16,
				["INVTYPE_WEAPONOFFHAND"] = 17,
				["INVTYPE_2HWEAPON"] = 16,
				["INVTYPE_SHIELD"] = 17,
				["INVTYPE_HOLDABLE"] = 17,
				["INVTYPE_THROWN"] = 18,
				["INVTYPE_RANGED"] = 18,
				["INVTYPE_RELIC"] = 18,
				["INVTYPE_RANGEDRIGHT"] = 18
			}
	return slotMap
end