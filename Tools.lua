Horizons.Tools = {}
Horizons.Tools.SoundList = {"jingle.mp3"}
Horizons.Tools.CmdHandlers = {}

function Horizons.Tools:InitCmdHandlers()
	Horizons.Tools:RegisterCmdHandler("TAKEPARTY", Horizons.Group.HandleTakeParty)
	Horizons.Tools:RegisterCmdHandler("TAKEALT", Horizons.Group.HandleTakeAlt)
	Horizons.Tools:RegisterCmdHandler("MESSAGE", 	function(input, sender)
																					if(input[2]) then
																						Horizons:Print(input[2] .. " (from "..sender..")")
																					end
																				end)
	Horizons.Tools:RegisterCmdHandler("LOCKCHANNELS", Horizons.Comms.HandleLock)
	Horizons.Tools:RegisterCmdHandler("UNLOCKCHANNELS", Horizons.Comms.HandleUnlock)
	Horizons.Tools:RegisterCmdHandler("PROFESSION", Horizons.Tools.HandleProfessionCheck)
	Horizons.Tools:RegisterCmdHandler("TIMER", Horizons.Tools.HandleTimer)
	Horizons.Tools:RegisterCmdHandler("VERSION", Horizons.Tools.HandleVersionCheck)
	Horizons.Tools:RegisterCmdHandler("SOUND", Horizons.Tools.HandleSound)
	Horizons.Tools:RegisterCmdHandler("ALERT", Horizons.Tools.HandleAlert)
	Horizons.Tools:RegisterCmdHandler("LOOKUP", 	function(input, sender)
																				if(Horizons.Config:IsOfficer(sender)) then
																					--look what up?
																					if(input[2]) then
																						if(type(input[2]) == "string") then
																							if(input[2] == "MAIN") then
																								Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {"My main character is: '" .. Horizons.Config:GetCharacterMainName().."'"})
																								--Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."My main character is: '" .. Horizons.Config:GetCharacterMainName().."'", "WHISPER", sender)
																							end
																						end
																					end
																				end
																			end)
	Horizons.Tools:RegisterCmdHandler("VOTESTART", Horizons.Tools.HandleVoteStart)
	Horizons.Tools:RegisterCmdHandler("LOCATE", Horizons.Tools.HandleLocate)
	Horizons.Tools:RegisterCmdHandler("LOCATERESPONSE", Horizons.Tools.HandleLocateResponse)
	Horizons.Tools:RegisterCmdHandler("AUCTIONSTART", Horizons.Tools.HandleAuctionStart)
	Horizons.Tools:RegisterCmdHandler("HANDLEBID", Horizons.Tools.HandleBid)
	Horizons.Tools:RegisterCmdHandler("SYNCICONASSIGNMENTS", Horizons.IconAssignments.HandleInput)
	
end

function Horizons.Tools:GetCmdFuncList()
	-- if(false) then
		-- Horizons.Tools.CmdFuncList = {
			-- ["TAKEPARTY"] = Horizons.Group.HandleTakeParty,
			-- ["TAKEALT"] = Horizons.Group.HandleTakeAlt,
			-- ["MESSAGE"] = function(input, sender)
				-- if(input[2]) then
					-- Horizons:Print(input[2] .. " (from "..sender..")")
				-- end
			-- end,
			-- ["LOCKCHANNELS"] = Horizons.Comms.HandleLock,
			-- ["UNLOCKCHANNELS"] = Horizons.Comms.HandleUnlock,
			-- ["PROFESSION"] = Horizons.Tools.HandleProfessionCheck,
			-- ["TIMER"] = Horizons.Tools.HandleTimer,
			-- ["VERSION"] = Horizons.Tools.HandleVersionCheck,
			-- ["SOUND"] = Horizons.Tools.HandleSound,
			-- ["ALERT"] = Horizons.Tools.HandleAlert,
			-- ["LOOKUP"] = function(input, sender)
				-- if(Horizons.Config:IsOfficer(sender)) then
					---- look what up?
					-- if(input[2]) then
						-- if(type(input[2]) == "string") then
							-- if(input[2] == "MAIN") then
								-- Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {"My main character is: '" .. Horizons.Config:GetCharacterMainName().."'"})
								---- Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."My main character is: '" .. Horizons.Config:GetCharacterMainName().."'", "WHISPER", sender)
							-- end
						-- end
					-- end
				-- end
			-- end,
			-- ["VOTESTART"] = Horizons.Tools.HandleVoteStart,
			-- ["LOCATE"] = Horizons.Tools.HandleLocate,
			-- ["LOCATERESPONSE"] = Horizons.Tools.HandleLocateResponse,
			-- ["AUCTIONSTART"] = Horizons.Tools.HandleAuctionStart,
			-- ["HANDLEBID"] = Horizons.Tools.HandleBid,
			-- ["SYNCICONASSIGNMENTS"] = Horizons.IconAssignments.HandleInput
		-- }
	-- end
	return Horizons.Tools.CmdHandlers
end


function Horizons.Tools:RegisterCmdHandler(handle, callback)
	if(type(handle) == "string" and type(callback) == "function") then
		Horizons.Tools.CmdHandlers[handle] = callback
	end
end

function Horizons.Tools:HandleTimerCountDown(endtime)
	local time_now = time()
	if(endtime <= time_now) then
		Horizons:CancelTimer(Horizons.Tools.TimerHandle, true)
		Horizons.Tools.TimerHandle = nil
		UIErrorsFrame:AddMessage( "Timer Completed!", 1.0, 1.0, 1.0, 1.0, 3)
	else
		UIErrorsFrame:AddMessage( "Time Left: " .. endtime - time_now, 1.0, 1.0, 1.0, 1.0, 3)
	end	
end

function Horizons.Tools.HandleTimer(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		if(input[2]) then
			local timerval = tonumber(input[2])
			if(type(timerval) == "number") then
				local endtime = timerval+time()
				if (DBM) then
					DBM:CreatePizzaTimer(timerval, "HorizonsTimer", false)
				else
					if(Horizons.Tools.TimerHandle) then
						Horizons:CancelTimer(Horizons.Tools.TimerHandle, true)
						Horizons.Tools.TimerHandle = nil
					end
					Horizons.Tools.TimerHandle = Horizons:ScheduleRepeatingTimer(function() Horizons.Tools:HandleTimerCountDown(endtime) end, 1)
				end
			end
		end
	end
end

function Horizons.Tools.HandleSound(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		if(Horizons.db.global.Settings.General.PlaySoundFiles) then
			if(input[2]) then
				local sound = tonumber(input[2])
				if(type(sound) == "number") then
					Horizons.Tools:PlaySound(sound)
				end
			end
		end
	end
end

function Horizons.Tools:PlaySound(index)
	if(Horizons.Tools.SoundList[index]) then
		PlaySoundFile("Interface\\Addons\\Horizons\\Media\\"..Horizons.Tools.SoundList[index])
	end
end

function Horizons.Tools.HandleVersionCheck(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		if(input[2]) then
			local version = tonumber(input[2])
			if(type(version) == "number") then
				if(version > Horizons.Version) then
					Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."I have an old version! ("..Horizons.Version..")", "WHISPER", sender)
					local link = nil
					if(input[3]) then
						link = input[3]
					end
					Horizons.Tools.PrintNewVersionAvailable(version, link)
					if(Horizons.Tools.VersionCheckTimerHandle) then
						Horizons:CancelTimer(Horizons.Tools.VersionCheckTimerHandle, true)
						Horizons.Tools.VersionCheckTimerHandle = nil
					end
					Horizons.Tools.VersionCheckTimerHandle = Horizons:ScheduleRepeatingTimer(function() Horizons.Tools.PrintNewVersionAvailable(version, link) end, (15*60))
				end
			end
		end
	end
end

function Horizons.Tools.PrintNewVersionAvailable(version, link)
	Horizons:Print("You have an old version of the addon! Version "..version.." is available!")
	if(link) then
		Horizons:Print("Link: " .. Horizons.Comms.CreateURLLink(link))
	end
	Horizons.Tools.ShowNotification("Horizons has been updated!", "Version "..version.." is available!")
end

function Horizons.Tools:GearCheck(player)
	local SLOTLIST = { 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }
	local itemValuePool = 0
	local numSlots = #SLOTLIST

	local inRange = CheckInteractDistance(player, 1)
	if not inRange then
		return player, "Out of range"
	end
	if player then
		NotifyInspect(player)
	else
		return player, "Player not found"
	end

	for i, slot in pairs(SLOTLIST) do
		local itemLink = GetInventoryItemLink(player, slot )
		if(itemLink ~= nil) then	
			local itemName, _, _, itemLevel, _, _, _, _, itemEquipLoc = GetItemInfo(itemLink)
			if itemEquipLoc == "INVTYPE_2HWEAPON" then
				if(GetInventoryItemLink(player, 17) == nil) then
					numSlots = numSlots - 1
				end
			end			
			itemValuePool = itemValuePool + itemLevel
		end		
	end
	return player, string.format( "%.2f", (itemValuePool / numSlots) )
end

function Horizons.Tools.HandleSlashGear(input)
	if(input == nil or input == "") then
		Horizons:Print("Your average item level is: " .. select(2,Horizons.Tools:GearCheck("player")))
	elseif(input == "target") then
		local target = UnitName("target")
		if(target) then
			local player, ilvlavg = Horizons.Tools:GearCheck(target)
			Horizons:Print(target .. " average item level is: " .. ilvlavg)
		end
	end
end

function Horizons.Tools.HandleVoteStart(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		--tjekker for minimum size
		if(#input >= 4) then
			local Question = ""
			local Answers = {}
			for i = 2, #input do 
				--valider on the fly
				if(type(input[i]) ~= "string") then return end
				local line = {strsplit(":", input[i])}
				if(#line ~= 2) then return end
				if(line[1] == "Q") then
					Question = line[2]
				elseif(line[1] == "A") then
					table.insert(Answers, line[2])
				end
			end
			if(Question ~= "" and #Answers >= 2) then
				
				local AceGUI = LibStub("AceGUI-3.0")
				local frame = AceGUI:Create("Window")
				frame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end)
				frame:SetTitle("Voting Window")
				frame:SetLayout("List")
				frame:SetWidth(450)
				frame:SetHeight(200)
				frame:SetAutoAdjustHeight(true)
				
				local label = AceGUI:Create("Label")
				label:SetText(Question)
				label:SetFont("Fonts\\FRIZQT__.TTF", 25)
				label:SetRelativeWidth(1)
				frame:AddChild(label)
				
				local group = AceGUI:Create("InlineGroup")
				group:SetLayout("List")
				group:SetRelativeWidth(1)
				frame:AddChild(group)
				
				--Horizons:Print2("Question: " .. Question)
				for i = 1, #Answers do
					local Abtn = AceGUI:Create("Button")
					Abtn:SetText(Answers[i])
					Abtn:SetRelativeWidth(1)
					Abtn:SetCallback("OnClick", function() Horizons.Tools.DoVote(i .. ":" .. sender); AceGUI:Release(frame) end)
					group:AddChild(Abtn)
					--Horizons:Print2("\124cffCC0000\124HCVVOTE:" .. i .. ":" .. sender .. "\124h[" .. Answers[i] .. "]\124h\124r ")
				end
				frame:Show()
			end
		end
	end
end

function Horizons.Tools.DoVote(link)
	if(link) then
		local info = {strsplit(":", link)}
		if(#info == 2) then
			--svar
			--1 = id
			--2 = voteholder
			Horizons.Comms:SendAddonMessage("VOTEANSWER", "WHISPER", info[2], {info[1]})
		end
	end
end

function Horizons.Tools:FormatText(text)
	if(not Horizons.Tools.ColorMap) then Horizons.Tools:CreateColorMap() end
	for key, value in pairs(Horizons.Tools.ColorMap) do
	    local fullTag = "_"..key.."_"
	    text = string.gsub( text, fullTag, value )
    end
	return text
end

function Horizons.Tools:CreateColorMap()
	if(not Horizons.Tools.ColorMap) then
		Horizons.Tools.ColorMap = {
			["END"] = "|r",
			["RESET"] = "|r",	
			["RED"] = "|cffFF0000",
			["GREEN"] = "|cff00FF00",
			["GRN"] = "|cff00FF00",
			["BLUE"] = "|cff0000FF",
			["BLU"] = "|cff0000FF",
			["YELLOW"] = "|cffFFFF00",
			["YEL"] = "|cffFFFF00",
			["TEAL"] = "|cff00FFFF",
			["PURPLE"] = "|cffFF00FF",
			["PUR"] = "|cffFF00FF",
			["WHITE"] = "|cffFFFFFF",
			["WHI"] = "|cffFFFFFF",
			["BLACK"] = "|cff000000",
			["BLA"] = "|cff000000",
			["GREY"] = "|cff7F7F7F",
			["GRAY"] = "|cff7F7F7F",
			["GRE"] = "|cff7F7F7F",
			["GRA"] = "|cff7F7F7F",
			["AOE"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("MAGE")),
			["MAGES"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("MAGE")),
			["MAGE"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("MAGE")),
			["MAG"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("MAGE")),
			["WARLOCKS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARLOCK")),
			["WARLOCK"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARLOCK")),
			["LOCK"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARLOCK")),
			["LOC"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARLOCK")),
			["MELEE"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("ROGUE")),
			["ROGUES"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("ROGUE")),
			["ROGUE"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("ROGUE")),
			["ROG"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("ROGUE")),
			["DRUIDS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("DRUID")),
			["DRUID"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("DRUID")),
			["DRU"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("DRUID")),
			["PRIESTS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PRIEST")),
			["PRIEST"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PRIEST")),
			["PRI"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PRIEST")),
			["HEALERS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PALADIN")),
			["HEALER"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PALADIN")),
			["PALADINS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PALADIN")),
			["PALADIN"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PALADIN")),
			["PAL"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("PALADIN")),
			["WARRIORS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["WARRIOR"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["WAR"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["TANKS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["TANK"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["OFFTANKS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["OFFTANK"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["OTS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["OT"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("WARRIOR")),
			["RANGED"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("HUNTER")),
			["HUNTERS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("HUNTER")),
			["HUNTER"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("HUNTER")),
			["HUN"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("HUNTER")),
			["SHAMANS"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("SHAMAN")),
			["SHAMAN"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("SHAMAN")),
			["SHA"] = Horizons.Tools:RGBtoHEX(Horizons.Config:GetStandardColor("SHAMAN")),
			["UNCOMMON"] = Horizons.Tools:GetItemQualityColor(2),
			["RARE"] = Horizons.Tools:GetItemQualityColor(3),
			["EPIC"] = Horizons.Tools:GetItemQualityColor(4),
			["EPIX"] = Horizons.Tools:GetItemQualityColor(4),
			["LEGENDARY"] = Horizons.Tools:GetItemQualityColor(5),
			["ARTIFACT"] = Horizons.Tools:GetItemQualityColor(6),
			["HTML"] = "|cff"
		}
	end
end

function Horizons.Tools:RGBtoHEX(r, g, b)
	r = r <= 1 and r >= 0 and r or 0
	g = g <= 1 and g >= 0 and g or 0
	b = b <= 1 and b >= 0 and b or 0
	return "|cff"..string.format("%02x%02x%02x", r*255, g*255, b*255)
end

function Horizons.Tools:GetItemQualityColor(level)
	local r,g,b,hex = GetItemQualityColor( level )
    if hex then
        return hex
    else
        return "|cff7F7F7F"
    end
end

function Horizons.Tools.PrintAllArenaTeams()
	
	for i = 1, 3 do
		local teamName, teamSize, teamRating, weekPlayed, weekWins, seasonPlayed, seasonWins, playerPlayed, seasonPlayerPlayed, teamRank, playerRating = GetArenaTeam(i)
		if teamSize > 0 then
			Horizons.Tools:PrintArenaLine( teamSize, teamRating, ((weekPlayed > 9) and ((playerPlayed/weekPlayed) >= 0.3)), Horizons.Tools:ArenaRatingToPoints( teamSize, teamRating ), 0)
		end
	end

end

function Horizons.Tools:PrintArenaLine( teamSize, teamRating, teamPlayerGets, teamPoints, mostPoints)
	local RATING     = "|cFFFF0000"
    local POINTS     = "|cFF00AF00"
    local POINTSBAD  = "|cFF5F5F5F"
    local POINTSMOST = "|cFF00FF00"

    local msg = ""

    local color = POINTSBAD
    if teamPlayerGets then
        color = POINTS
    end
    if tonumber(teamPoints) == tonumber(mostPoints) then
        color = POINTSMOST
    end
    
    msg = msg..teamSize.."v"..teamSize.." team : "
    msg = msg..RATING..teamRating.."|r  -> "
    msg = msg..color..teamPoints.."|r points"
    
    Horizons:Print2(msg)
end

function Horizons.Tools:ArenaRatingToPoints( teamsize, rating )
    local TEAMSCALE = { 0, 0.76, 0.88, 0, 1.0 }
    local points
    if rating > 1500 then
        points = 1511.26 / (1 + 1639.28 * math.exp(-0.00412*rating) )
    else
        points = 0.22 * rating + 14
    end
    return string.format( "%4.0f", math.floor(points * TEAMSCALE[teamsize]) )
end

function Horizons.Tools.HandleAuctionStart(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		Horizons:Print("======================================")
		Horizons:Print("\124cff00FF00\124HCVAUCTION:" .. input[2] .. ":" .. sender .. "\124h[ CLICK HERE IF YOU ARE INTERESTED! ]\124h\124r ")
		Horizons:Print("======================================")
		Horizons.Tools:ShowAlert("NEW ITEM ON AUCTION!", sender)
	end
end


function Horizons.Tools.ShowNotification(topLineText, bottomLineText)
	local topLine = BNToastFrameTopLine;
	local bottomLine = BNToastFrameBottomLine;
	BNToastFrameIconTexture:SetTexCoord(0, 0.25, 0.5, 1);
	topLine:Show();
	topLine:SetFormattedText(topLineText);
	topLine:SetTextColor(FRIENDS_BNET_NAME_COLOR.r, FRIENDS_BNET_NAME_COLOR.g, FRIENDS_BNET_NAME_COLOR.b);
	bottomLine:Show();
	bottomLine:SetText(bottomLineText);
	bottomLine:SetTextColor(FRIENDS_GRAY_COLOR.r, FRIENDS_GRAY_COLOR.g, FRIENDS_GRAY_COLOR.b);
	BNToastFrameDoubleLine:Hide();
		
	local frame = BNToastFrame;
	BNToastFrame_UpdateAnchor(true);
	frame:Show();
	PlaySoundKitID(18019);
	frame.animIn:Play();
	BNToastFrameGlowFrame.glow.animIn:Play();
	frame.waitAndAnimOut:Stop();	--Just in case it's already animating out, but we want to reinstate it.
	if ( frame:IsMouseOver() ) then
		frame.waitAndAnimOut.animOut:SetStartDelay(1);
	else
		frame.waitAndAnimOut.animOut:SetStartDelay(frame.duration);
		frame.waitAndAnimOut:Play();
	end
end


--[[
	Handler for calculator slash command.
]]
function Horizons.Tools.Calculate(cmd)
	if cmd == "" then
		Horizons:Print( "/calc expression (ie. |cffFFFFFF/calc 5+5*2|r)" )
	elseif cmd == "check" and Horizons.Tools.CalcAnswer ~= nil then
		Horizons:Print("x = " .. Horizons.Tools.CalcAnswer )
	elseif cmd == "check" and Horizons.Tools.CalcAnswer == nil then
		Horizons:Print("x has no value." )
	else
		-- Commandline calculation
		output = Horizons.Tools.CalculateExpression( cmd )
		Horizons:Print( cmd.." = "..output )
	end
end

--[[
	Calculates an expression.
]]
function Horizons.Tools.CalculateExpression( expression )
	if expression ~= "" then
		local expcheck = Horizons.Tools.CheckExpression( expression )
		if expcheck ~= "" then return expcheck;	end
		expression = Horizons.Tools.FormulaSub( expression )
		local tvar = "tempvar"
		setglobal( tvar, nil )
		RunScript( tvar .. "=("..expression..")" )
		local result = getglobal(tvar)
		Horizons.Tools.CalcAnswer = result
		return result
	else
		local error = "Error, invalid expression"
		return error
	end
end

--[[
	Check a math expression
]]
function Horizons.Tools.CheckExpression( expression )
	-- The massive formula error checker!
	local error = nil
	-- Invalid character check
	for w in string.gmatch(expression, "[a-wy-zA-Z%c%z%s~`!@#%$%%&_|%[{%]};:'\"<,>?\]") do
		error = "Error, invalid characters"
		return error
	end
	-- Decimal check
	if string.find(expression, "%d+%.%d+%.") ~= nil or string.find(expression, "%.%d+%.") ~= nil or string.find(expression, "%.%.") ~= nil then
		error = "Error, incorrect decimal format"
		return error
	end
	-- Bracket checks
	local brkcount = 0
	if string.find(expression, "()", 1, 1) ~= nil then
		error = "Error, brackets not balanced"
		return error
	end
	for x in string.gmatch(expression, "[()]") do
		if x == "(" then
			brkcount = brkcount+1
		elseif x == ")" then
			brkcount = brkcount-1
		end
		if brkcount < 0 then
			error = "Error, brackets not balanced"
			return error
		end
	end
	if brkcount ~= 0 then
		error = "Error, brackets not balanced"
		return error
	end
	-- Invalid operator placement check
	local expsub, expsub2 = ""
	expsub = string.sub(expression, 1, 1)
	expsub2 = string.sub(expression, -1, -1)
	if string.find(expsub, "[%+%*%^/]") ~= nil then
		error = "Error, invalid operators"
		return error
	elseif string.find(expsub2, "[%+%*%^%-/]") ~= nil then
		error = "Error, invalid operators"
		return error
	end
	-- Double operator check
	if string.find(expression, "[%+%*%^%-/][%+%*%^%)/]") ~= nil then
		error = "Error, repeated operator"
		return error
	end
	-- x value check
	-- At this point the only letter in the equation would be x
	-- This checks for that letter and makes sure a value has been saved
	if string.find(expression, "x") ~= nil then
		if Horizons.Tools.CalcAnswer == nil then
			error = "Error, previous answer not defined"
			return error
		end
	end
	-- No errors!
	error = ""
	return error
end

--[[
	
]]
function Horizons.Tools.FormulaSub(expression)
	-- This function replaces values in the expression
	-- Check for --, replace with +
	expression = string.gsub( expression, "%-%-", "+" )
	-- Check for nx ny nz etc, replace with n*x n*y n*z etc
	expression = string.gsub( expression, "([x])(%d)", "%1%*%2" )
	expression = string.gsub( expression, "([x])%.", "%1%*%." )
	expression = string.gsub( expression, "(%d)([x])", "%1*%2" )
	expression = string.gsub( expression, "%.([x])", "%.*%1" )
	----This must be done twice due to LUA's gsub quirk
	expression = string.gsub( expression, "([x])([x])", "%1%*%2" )
	expression = string.gsub( expression, "([x])([x])", "%1%*%2" )
	-- Check for n( or )n and replace with n*( or )*n
	expression = string.gsub( expression, "([%dx])%(", "%1%*%(" )
	expression = string.gsub( expression, "%)([%dx])", "%)%*%1" )
	expression = string.gsub( expression, "%)%(", "%)%*%(" )
	expression = string.gsub( expression, "%.%(", "%.%*%(" )
	expression = string.gsub( expression, "%)%.", "%)%*%." )
	-- Substitute x with previous answer
	if Horizons.Tools.CalcAnswer ~= nil then
		expression = string.gsub( expression, "x", Horizons.Tools.CalcAnswer )
	end
	return expression
end

function Horizons.Tools.HandleSlashVentrilo(input)
	local ip = "78.47.29.3"
	local port = "3600"
	if(input ~= nil and input ~= "") then
		SendChatMessage("IP: " .. ip, "WHISPER", nil, input)
		SendChatMessage("PORT: " .. port, "WHISPER", nil, input)
	else
		Horizons:Print(Horizons.Comms.CreateURLLink("IP: " .. ip .. " " .. "PORT: " .. port))
	end
end

function Horizons.Tools.ShowInformationPopup(text)
	if not StaticPopupDialogs["HZPOPUP"] then
		StaticPopupDialogs["HZPOPUP"] = {
				text = "",
				button1 = OKAY,
				timeout = 0,
				hideOnEscape = 1,
				whileDead = 1
		}
	end
	StaticPopupDialogs["HZPOPUP"].text = text
	StaticPopup_Show("HZPOPUP")
end







