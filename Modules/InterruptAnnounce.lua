Horizons.Modules:RegisterModule(function()
	local module = {}
			module.Name = "Interrupt Announce"
			module.Description = "Announces in say chat when you interrupt a spell"
			module.DefaultOptions = {
				usecustommessage = {
					type = "toggle",
					name = "Use Custom Message",
					desc = "Click this to toggle",
					width = "double",
					order = 1,
					set = function(info, val) module:SetUseCustomMessage(val) end,
					get = function() return module:IsUseCustomMessage() end
				},
				custommessage = {
					type = "input",
					name = "Custom Message",
					desc = "Custom",
					width = "full",
					multiline = false,
					usage = "Interrupted %s",
					order = 2,
					disabled = function() return not (module:IsUseCustomMessage() and Horizons.Modules:GetModuleState(module.Name)) end,
					set = function(info, val) module:SetCustomMessage(val) end,
					get = function() return module:GetCustomMessage() end
				},
				onlydisplayingroup = {
					type = "toggle",
					name = "Only announce while in group",
					desc = "Click this to toggle",
					width = "double",
					order = 1,
					set = function(info, val) module:SetIsAnnounceInGroupOnly(val) end,
					get = function() return module:IsAnnounceInGroupOnly() end
				}
			}
						
	function module:Enable()
		Horizons:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED", function(eventName, ...)
				local User = select(5, ...)
				if User ~= UnitName("player") then return end
				local Event = select(2, ...)
				local spellID = select(15, ...)
				if Event == "SPELL_INTERRUPT" then
					local saymsg = "%s interrupted"
					if module:IsUseCustomMessage() then
						saymsg = module:GetCustomMessage()
					end
					if (module:IsAnnounceInGroupOnly() and not Horizons.Util.IsInParty()) then return end	
					SendChatMessage(string.format(saymsg, GetSpellLink(spellID)), "SAY")
				end
		end)
	end

	function module:Disable()
		Horizons:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	end
	
	function module:SetUseCustomMessage(val)
		Horizons.db.global.Settings.Modules[module.Name].UseCustomMessage = val
	end
	
	function module:IsUseCustomMessage()
		if Horizons.db.global.Settings.Modules[module.Name].UseCustomMessage then
			return Horizons.db.global.Settings.Modules[module.Name].UseCustomMessage
		else
			return false
		end
	end
	
	function module:SetCustomMessage(msg)
		Horizons.db.global.Settings.Modules[module.Name].CustomMessage = msg
	end
	
	function module:GetCustomMessage()
		if Horizons.db.global.Settings.Modules[module.Name].CustomMessage then
			return Horizons.db.global.Settings.Modules[module.Name].CustomMessage
		else 
			return "%s interrupted"
		end
	end
	
	function module:SetIsAnnounceInGroupOnly(val)
		Horizons.db.global.Settings.Modules[module.Name].IsAnnounceInGroupOnly = val
	end
	
	function module:IsAnnounceInGroupOnly()
		if Horizons.db.global.Settings.Modules[module.Name].IsAnnounceInGroupOnly then
			return Horizons.db.global.Settings.Modules[module.Name].IsAnnounceInGroupOnly
		else
			return false
		end
	end

	return module
end
)
