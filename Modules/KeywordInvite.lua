Horizons.Modules:RegisterModule(function()
	local module = {}
		  module.Name = "Keyword Invite"
		  module.Description = "Keyword lol"
		  module.note = "Auto convert to raid & Set keyword"
		  module.DefaultOptions = {
				keywords = {
					type = "input",
					name = "Keywords (Seperated by ' '(whitespace))",
					desc = "Enter the keywords you want the module to react on",
					width = "full",
					multiline = false,
					pattern = "",
					usage = "inv invite etc",
					order = 1,
					set = function(info, val) module:SetKeywords(val) end,
					get = function() return module.GetKeywords() end
				},
				isautoconvert = {
					type = "toggle",
					name = "Automatically convert to raid",
					desc = "Automatically converts to raid when party is full",
					width = "double",
					order = 2,
					set = function(info, val) module:SetIsAutoConvert(val) end,
					get = function() return module.IsAutoConvert() end
				},
				onlyinviteguildies = {
					type = "toggle",
					name = "Only invite guildies",
					desc = "Only invite guildies, ignores everyone else.",
					width = "double",
					order = 3,
					set = function(info, val) module:SetIsOnlyInviteGuildies(val) end,
					get = function() return module.IsOnlyInviteGuildies() end
				}
			}

		function module:Enable()
			Horizons:RegisterEvent("CHAT_MSG_WHISPER", function(eventName, ...) 
				local msg = select(1, ...)
				local sender = select(2, ...)
				local keywords = {string.split(" ", module:GetKeywords())}
					for k,v in pairs(keywords) do
						if (string.lower(msg) == string.lower(v)) then
							if (not module:IsOnlyInviteGuildies() or Horizons.WowApi.UnitIsInMyGuild(sender)) then
								if (module:IsAutoConvert() and Horizons.WowApi.GetNumGroupMembers() == 4 and not Horizons.WowApi.IsInRaid()) then
									ConvertToRaid()
								end
								InviteUnit(sender)
								return
							end
						end
					end
			end)
		end

		function module:Disable()
			Horizons:UnregisterEvent("CHAT_MSG_WHISPER")
		end
		
		function module:SetKeywords(val)
			Horizons.db.global.Settings.Modules[module.Name].Keywords = val
		end
		
		function module:GetKeywords()
			if Horizons.db.global.Settings.Modules[module.Name].Keywords then
				return Horizons.db.global.Settings.Modules[module.Name].Keywords
			else
				return "inv"
			end
		end
		
		function module:SetIsAutoConvert(val)
			Horizons.db.global.Settings.Modules[module.Name].AutoConvert = val
		end
		
		function module:IsAutoConvert()
			return Horizons.db.global.Settings.Modules[module.Name].AutoConvert
		end
		
		function module:SetIsOnlyInviteGuildies(val)
			Horizons.db.global.Settings.Modules[module.Name].onlyinviteguildies = val
		end
		
		function module:IsOnlyInviteGuildies()
			return Horizons.db.global.Settings.Modules[module.Name].onlyinviteguildies
		end

	return module
end
)
