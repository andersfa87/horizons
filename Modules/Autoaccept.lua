Horizons.Modules:RegisterModule(function()
	local module = {}
		  module.Name = "Auto Accept"
		  module.Description = "Automatically accept invites from guild members"

		function module:Enable()
			Horizons:RegisterEvent("PARTY_INVITE_REQUEST", function(eventName, ...) 
				if(UnitIsInMyGuild(select(1,...))) then
					AcceptGroup()
					Horizons:ScheduleTimer(function() StaticPopup_Hide("PARTY_INVITE") end, 3)
				end
			end)
		end

		function module:Disable()
			Horizons:UnregisterEvent("PARTY_INVITE_REQUEST")
		end

	return module
end
)
