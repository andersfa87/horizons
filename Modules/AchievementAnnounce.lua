Horizons.Modules:RegisterModule(function()
	local module = {}
		  module.Name = "Achievement Announce"
		  module.Description = "Announce achievements to horizons chat"

		function module:Enable()
			Horizons:RegisterEvent("ACHIEVEMENT_EARNED", function(eventName, ...) 
				Horizons.Comms:SendMessage("SHOUT", "Achievement " .. GetAchievementLink(select(1, ...)) .. " Earned!")
			end)
		end

		function module:Disable()
			Horizons:UnregisterEvent("ACHIEVEMENT_EARNED")
		end

	return module
end
)
