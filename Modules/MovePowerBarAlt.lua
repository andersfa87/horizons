-- Moves the poweraltbar to the top of the screen
Horizons.Modules:RegisterModule(function()
	local module = {}
		  module.Name = "MovePowerBarAlt"
		  module.Description = "Moves the alternative power bar to the top of the screen"

		function module:Enable()
			PlayerPowerBarAlt:SetMovable(true)
			PlayerPowerBarAlt:SetUserPlaced(true)
			PlayerPowerBarAlt:ClearAllPoints()
			PlayerPowerBarAlt:SetPoint("TOP", 0, -100)
		end

		function module:Disable()
			PlayerPowerBarAlt:SetUserPlaced(false)
			PlayerPowerBarAlt:ClearAllPoints()
			PlayerPowerBarAlt:SetMovable(false)
			Horizons.Tools.ShowInformationPopup("Reload your UI for the changes to take effect.")
		end

	return module
end
)
