-- Adds timestamps to the main chat
Horizons.Modules:RegisterModule(function()
	local module = {}
		  module.Name = "Timestamps"
		  module.Description = "Adds timestamps to the chat windows"
		  module.DefaultOptions = {
			color = {
				type = "color",
				name = "Pick Color",
				desc = "Pick a color for your timestamps",
				order = 1,
				set = function(info, r, g, b) module:SetCustomColor(r, g, b) end,
				get = function() return module:GetColor() end
			},
			reset = {
				type = "execute",
				name = "Reset color",
				desc = "Reset color back to standard",
				order = 2,
				func = function() module:ResetColor() end
			},
			showseconds = {
				type = "toggle",
				name = "Show seconds",
				desc = "Adds seconds to the timestamps",
				order = 3,
				set = function(info, val) module:SetShowSeconds(val) end,
				get = function() return module:IsShowSeconds() end
			},
			--[[usegametime = {
				type = "toggle",
				name = "Use game time",
				desc = "Use game time intead of local time",
				order = 4,
				disabled = true,
				set = function(info, val) end,
				get = function() return false end
			}]]
		  }
		  module.Format = "[%H:%M]"
		  module.FormatWithSecond = "[%X]"

		function module:Enable()
			for i = 1, NUM_CHAT_WINDOWS do
				local framex = getglobal( "ChatFrame"..i )
				Horizons:RawHook(framex, "AddMessage", function(frame, text, ...) self:AddMessage(frame, text, ...) end, true)
			end
		end

		function module:Disable()
			for i = 1, NUM_CHAT_WINDOWS do
				local framex = getglobal( "ChatFrame"..i )
				Horizons:Unhook(framex, "AddMessage")
			end
		end
		
		function module:AddMessage(frame, text, ...)
			Horizons.hooks[frame].AddMessage(frame, self:GetTimeStamp() .. text, ...)
		end
		
		function module:GetTimeStamp()
			local format = self.Format
			if (Horizons.db.global.Settings.Modules[self.Name].ShowSeconds) then
				format = self.FormatWithSecond
			end
			local tempdate = date("*t")
			return "\124cff" .. Horizons.Util:RGBtoHEX(self:GetColor()) .. date(format, time(tempdate)) .. "\124r"
		end
		
		function module:GetStandardColor()
			return 0.4, 0.4, 0.4
		end
		
		function module:SetCustomColor(red, green, blue)
			Horizons.db.global.Settings.Modules[self.Name].CustomColor = {r = red, g = green, b = blue}
		end
		
		function module:GetColor()
			if (Horizons.db.global.Settings.Modules[self.Name].CustomColor) then
				return Horizons.db.global.Settings.Modules[self.Name].CustomColor.r, Horizons.db.global.Settings.Modules[self.Name].CustomColor.g, Horizons.db.global.Settings.Modules[self.Name].CustomColor.b
			else
				return self:GetStandardColor()
			end
		end
		
		function module:ResetColor()
			Horizons.db.global.Settings.Modules[self.Name].CustomColor = nil
		end
		
		function module:SetShowSeconds(val)
			Horizons.db.global.Settings.Modules[self.Name].ShowSeconds = val
		end
		
		function module:IsShowSeconds()
			return Horizons.db.global.Settings.Modules[self.Name].ShowSeconds
		end

	return module
end
)
