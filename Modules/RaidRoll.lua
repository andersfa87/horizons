Horizons.Modules:RegisterModule(function()
	local module = {}
		  module.Name = "Raid Roller"
		  module.Description = "Raid Roller"
		  module.RaidRollActive = false

		function module:Enable()
			module.PlayerName = UnitName("player")
			Horizons:RegisterEvent("CHAT_MSG_SYSTEM", function(eventName, ...) 
				if(module.RaidRollActive) then
					local text = select(1, ...)
					if(string.sub(text, 1, string.len(module.PlayerName)+6) == module.PlayerName .. " rolls") then
						local roll = strmatch(text, "(%d+)")
						roll = tonumber(strtrim(roll))
						if(roll == nil) then
							Horizons:Print("Error: Roll value not found")
						elseif(roll <= Horizons.WowApi.GetNumGroupMembers()) then
							local winner = select(1, GetRaidRosterInfo(roll))
							SendChatMessage("Winner is *" .. winner .. "*! Congratulations!" , "RAID", nil, nil)
							module.RaidRollActive = false
						end
					end
				end
			end)
			Horizons:RegisterChatCommand("raidroll", function(input)
				if not(Horizons.WowApi.IsInRaid()) then
					Horizons:Print("Error: Not in a raid")
					return
				end
				
				if not(module.RaidRollActive) then
					module.RaidRollActive = true
					local number_raid_members = Horizons.WowApi.GetNumGroupMembers()
					SendChatMessage("========Horizons Raid Roller!!========" , "RAID", nil, nil);
					local toPrint = ""
					for i = 1, number_raid_members do
							local name = select(1,GetRaidRosterInfo(i))
							toPrint = toPrint .. "[" .. i .. ". " .. name.."] "
							
							if ( i%5 == 0 ) then
								SendChatMessage(toPrint , "RAID", nil, nil)
								toPrint = ""
							end
					end	
					if ( toPrint ~= "" ) then
						SendChatMessage(toPrint , "RAID", nil, nil)
					end
					SendChatMessage("========================" , "RAID", nil, nil)
					RandomRoll(1, number_raid_members)
				end
			end)
		end

		function module:Disable()
			Horizons:UnregisterEvent("PARTY_INVITE_REQUEST")
		end
		
	return module
end
)
