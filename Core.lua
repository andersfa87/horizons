Horizons = LibStub("AceAddon-3.0"):NewAddon("Horizons", "AceConsole-3.0", "AceComm-3.0", "AceEvent-3.0", "AceHook-3.0", "AceTimer-3.0")
Horizons.Version = 0.899

--[[ OnInitialize()
]]
function Horizons:OnInitialize()
  self.Config:InitialiseDB()
  self:RegisterChatCommand("hzreset", function() Horizons.db:ResetDB(defaultProfile) ConsoleExec("reloadui") end)
  self.Config:InitialiseConfigGui()
  self:RawHook("SetItemRef", true)
  self:RawHook("SendChatMessage", true)
  self:RegisterComm("Horizons", "ON_COMM_RECIEVED")
  self.Tools:InitCmdHandlers()
  self:RegisterEvents()
  
  Horizons.Config:AddOfficer("Taziator")
	Horizons.Config:AddOfficer("Ultradude")
	Horizons.Config:AddOfficer("Cardoric")
	Horizons.Config:AddOfficer("Taop")

  Horizons.Config:AddOfficer("Wickey")
  
  Horizons.Config:AddOfficer("D\øduder")
	Horizons.Config:AddOfficer("Viente")
  
  Horizons.Config:AddOfficer("Moumouni")
  
  Horizons.Config:AddOfficer("Phr")
  	Horizons.Config:AddOfficer("Gingerrage")
  
  Horizons.Config:AddOfficer("Fendi")

  Horizons.Config:AddOfficer("Durndusthorn")

  Horizons.Config:AddOfficer("Maliku")

  Horizons.Config:AddOfficer("Jemoeder")

  Horizons.Config:AddOfficer("Munloz")
  
  self:RegisterChatCommands()
  self:Print("Horizons Addon("..self.Version..") Initialised")
  self.Modules:Initialise()
end


--[[ RegisterEvents()
]]
function Horizons:RegisterEvents()
	self:RegisterEvent("CHAT_MSG_CHANNEL")
	self:RegisterEvent("CHAT_MSG_CHANNEL_NOTICE")
end


--[[ RegisterChatCommands()
]]
function Horizons:RegisterChatCommands()
	
	self:RegisterChatCommand("hzlink", function(input) Horizons.Comms:SendURL(input) end)
	self:RegisterChatCommand("hzconf", function() InterfaceOptionsFrame_OpenToCategory("Horizons") end)
	self:RegisterChatCommand("hzconfig", function() InterfaceOptionsFrame_OpenToCategory("Horizons") end)
		
	--GLOBAL
	self:RegisterSlashChannelHandler("SHOUT", {"/hz", "/hzshout"})
	
	--HEAL
	self:RegisterSlashChannelHandler("HEAL", {"/heal", "/hzheal"})
	
	--TANK
	self:RegisterSlashChannelHandler("TANK", {"/tank", "/hztank"})
	
	--MAGE
	self:RegisterSlashChannelHandler("MAGE", {"/hzm", "/hzmage"})
	
	--SHAMAN
	self:RegisterSlashChannelHandler("SHAMAN", {"/hzs", "/hzshaman"})
	
	--DRUID
	self:RegisterSlashChannelHandler("DRUID", {"/hzd", "/hzdruid"})
	
	--ROGUE
	self:RegisterSlashChannelHandler("ROGUE", {"/hzr", "/hzrogue"})
	
	--WARRIOR
	self:RegisterSlashChannelHandler("WARRIOR", {"/hzw", "/hzwarrior"})
	
	--WARLOCK
	self:RegisterSlashChannelHandler("WARLOCK", {"/hzl", "/hzwarlock"})
	
	--PRIEST
	self:RegisterSlashChannelHandler("PRIEST", {"/hzpr", "/hzpriest"})
	
	--HUNTER
	self:RegisterSlashChannelHandler("HUNTER", {"/hzh", "/hzhunter"})
	
	--DEATHKNIGHT
	self:RegisterSlashChannelHandler("DEATHKNIGHT", {"/hzdk", "/hzdeathknight"})
	
	--PALADIN
	self:RegisterSlashChannelHandler("PALADIN", {"/hzb", "/hzpa", "/hzpaladin"})
  
	self:RegisterChatCommand("takeparty", Horizons.Group.TakeParty)
	self:RegisterChatCommand("takealt", Horizons.Group.TakeAlt)
	self:RegisterChatCommand("profession", Horizons.Tools.ProfessionCheck)
	self:RegisterChatCommand("prof", Horizons.Tools.ProfessionCheck)
	self:RegisterChatCommand("hzprof", Horizons.Tools.ProfessionCheck)
	self:RegisterChatCommand("gear", Horizons.Tools.HandleSlashGear)
	self:RegisterChatCommand("hzgear", Horizons.Tools.HandleSlashGear)
	self:RegisterChatCommand("arena", Horizons.Tools.PrintAllArenaTeams)
	self:RegisterChatCommand("hzarena", Horizons.Tools.PrintAllArenaTeams)
	self:RegisterChatCommand("calc", Horizons.Tools.Calculate)
	self:RegisterChatCommand("hztar", Horizons.IconAssignments.HandleSlashCommand)
	self:RegisterChatCommand("hzicon", Horizons.IconAssignments.HandleSlashCommand)
	self:RegisterChatCommand("hzcam", function() self:Print("Executing console command 'CameraDistanceMaxFactor 4'") ConsoleExec("CameraDistanceMaxFactor 4") end)
	self:RegisterChatCommand("reloadui", function() ConsoleExec("reloadui") end)
	self:RegisterChatCommand("hzvent", Horizons.Tools.HandleSlashVentrilo)
	self:RegisterChatCommand("locate", Horizons.Tools.HandleSlashLocate)
	
	self:RegisterChatCommand("newitem", Horizons.Tools.NewItemForAuction)
	
	if(Horizons.Officer and Horizons.Config:IsOfficer(UnitName("player"))) then
		Horizons.Officer.LoadOfficerStuff()
		self:RegisterChatCommand("hzofficer", function(input) Horizons.Officer:HandleSlashOfficer(input) end)
		self:RegisterChatCommand("hzo", function(input) Horizons.Officer:HandleSlashOfficer(input) end)
	end
end


--[[ RegisterSlashChannelHandler(channel, cmds)
]]
function Horizons:RegisterSlashChannelHandler(channel, cmds)
	if(type(channel) == "string" and type(cmds) == "table") then
		local class = Horizons:GetPlayerClass("player")
		local slash = "HZ"..channel
		setglobal("CHAT_"..slash.."_SEND", "HZ"..Horizons.Comms.ABBREVIATIONS[channel]..":\32 ")
		setglobal("CHAT_"..slash.."_GET", "HZ"..Horizons.Comms.ABBREVIATIONS[channel]..": %1\32 ")
		local r,g,b = Horizons.Config:GetChannelColor(channel, nil)
		ChatTypeInfo[slash] = { r=r, g=g, b=b, sticky = 1 }
		SlashCmdList["SLASH_"..slash] = function(input)   
			Horizons.Comms:SendMessage(channel, input)
		end
		
		for i = 1, #cmds do
			setglobal("SLASH_"..slash..i, cmds[i])
		end
		if(class == channel) then
			setglobal("SLASH_"..slash..(#cmds+1), "/cl")
		end
	end
end

function Horizons:Print2(msg)
	DEFAULT_CHAT_FRAME:AddMessage(Horizons.Tools:FormatText("_RED_<HZ>_END_ "..msg), 0.7,0.7,1)
end

--[[ GetPlayerClass(player)
]]
function Horizons:GetPlayerClass(player)
	local class = UnitClass(player)
	if (class == nil) then
		return ""
	end
	if (class == "Death Knight" or class == "DEATH KNIGHT") then
		class = "DEATHKNIGHT"
	end
	return string.upper(class)
end

--this is a hooked function
function Horizons:SetItemRef(link, text, button)
	if strsub(link, 1, 4) == "CURL" then
		Horizons.Comms:ShowURL(strsub(link,6))
	elseif strsub(link, 1, 6) == "CVVOTE" then
		Horizons.Tools.DoVote(strsub(link,8))
	elseif strsub(link, 1, 9) == "CVAUCTION" then
		Horizons.Tools.BidOnAuction(strsub(link,11))
	else
		self.hooks.SetItemRef(link, text, button)
	end
end

--this is a hooked function
function Horizons:SendChatMessage(msg, channel, lang, target)
	if(Horizons.Comms:IsViableChannel(string.sub(channel, 3))) then
		Horizons.Comms:SendMessage(string.sub(channel, 3), msg)
	else
		self.hooks.SendChatMessage(msg, channel, lang, target)
	end
end
