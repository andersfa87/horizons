Horizons.Comms = {}
Horizons.Comms.Name = "ConvergenceChannel"
Horizons.Comms.MultiMsgs = {}
Horizons.Comms.MultiSize = 200
Horizons.Comms.Separator = "�"
Horizons.Comms.ChannelList = {"SHOUT", "TANK", "HEAL", "DRUID", "PRIEST", "SHAMAN", "PALADIN", "WARRIOR", "DEATHKNIGHT", "HUNTER", "WARLOCK", "MAGE", "ROGUE"}
Horizons.Comms.ABBREVIATIONS = {	SHOUT = "", MAGE = "-M", SHAMAN = "-S", PRIEST = "-PR", PALADIN = "-PA", DRUID = "-D", WARRIOR = "-W", HUNTER = "-H", ROGUE = "-R", WARLOCK = "-L", DEATHKNIGHT = "-DK", HEAL = "-HEAL", TANK = "-TANK"}

--[[
	Initialises the comms system.
]]
function Horizons.Comms:Initialise()
	JoinChannelByName(Horizons.Comms.Name, nil, DEFAULT_CHAT_FRAME);
	ChatFrame_RemoveChannel(DEFAULT_CHAT_FRAME, Horizons.Comms.Name)
	Horizons:Print("Horizons chat active")
end

--[[ 
	Event handler for event "CHAT_MSG_CHANNEL".
]]
function Horizons:CHAT_MSG_CHANNEL(eventName, ...)
	if(select(9,...) == Horizons.Comms.Name) then
		local msg, sender = select(1, ...)
		if(string.sub(msg, 1, 2) == "S:") then
			local channel, actualmsg = string.match(Horizons.Comms:Decrypt(string.sub(msg, 3)), "(%a+):(.+)")
			if(channel == nil or actualmsg == nil) then return end
			Horizons.Comms:ProcessMessage(string.trim(channel), string.trim(actualmsg), sender)
		elseif(string.sub(msg, 1, 2) == "M:") then
			local part, parts, content = string.match(msg, "M:(%d+):(%d+):(.+)")
			if(part == nil or parts == nil or content == nil) then return end
			part = tonumber(part)
			parts = tonumber(parts)
			if(part == 1) then
				Horizons.Comms.MultiMsgs[sender] = {}
			end
			Horizons.Comms.MultiMsgs[sender][part] = content
			if(part == parts) then
				local msgbuild = ""
				for i = 1, parts do
					msgbuild = msgbuild..Horizons.Comms.MultiMsgs[sender][i]
				end
				local channel, actualmsg = string.match(Horizons.Comms:Decrypt(msgbuild), "(%a+):(.+)")
				Horizons.Comms:ProcessMessage(string.trim(channel), string.trim(actualmsg), sender)
				wipe(Horizons.Comms.MultiMsgs[sender])
			end
		end
	end
end

--[[
	Event handler for event "CHAT_MSG_CHANNEL_NOTICE".
]]
function Horizons:CHAT_MSG_CHANNEL_NOTICE()
	if not Horizons.Comms:IsJoined() then
		Horizons.Comms:Initialise()
	end
end

--[[ 
	Event handler for event "ACHIEVEMENT_EARNED".
]]
function Horizons:ACHIEVEMENT_EARNED(eventName, ...)
	
end

--[[ 
	Event handler for event "ON_COMM_RECIEVED".
	Handles incoming addon whispers.
]]
function Horizons:ON_COMM_RECIEVED(prefix, message, distribution, sender)
	local input = {strsplit(Horizons.Comms.Separator, message)}
	if(Horizons.Tools:GetCmdFuncList()[input[1]]) then
		Horizons.Tools:GetCmdFuncList()[input[1]](input, sender)
	end
end

--[[ 
	Prints a message to the addon chat channel.
	Will split in parts if message is too long.
]]
function Horizons.Comms:Send(prefix, msg)
	if(type(prefix) == "string" and type(msg) == "string") then				
		if(Horizons.Comms:IsJoined()) then		
			if(string.len(msg) < 200) then
				SendChatMessage("S:"..Horizons.Comms:Encrypt(string.upper(prefix)..":"..msg), "CHANNEL", nil, GetChannelName(Horizons.Comms.Name))
			else
				local parts = math.ceil(string.len(msg) / Horizons.Comms.MultiSize)
				local msg_remain = Horizons.Comms:Encrypt(string.upper(prefix)..":"..msg)
				for i = 1, parts do
					local thispart = string.sub(msg_remain, 1, Horizons.Comms.MultiSize)
					msg_remain = string.sub(msg_remain, Horizons.Comms.MultiSize+1)
					SendChatMessage("M:"..i..":"..parts..":"..thispart, "CHANNEL", nil, GetChannelName(Horizons.Comms.Name))
				end
			end
		else
			Horizons:Print("You are not connected, attempting to connect")
			Horizons.Comms:Initialise()
		end
	end	
end

--[[ 
	Sends a normal chat message to a channel.
]]
function Horizons.Comms:SendMessage(channel, msg)
	if(type(channel) == "string" and type(msg) == "string") then
		if(Horizons.Comms:IsViableChannel(channel)) then
			if(Horizons.db.global.Settings.Channel[channel].Visible) then	
				msg = " " .. msg
				--replace %t <target>
				if(string.find(msg, "%%t")) then
					local name = UnitName("target")
					if name == nil then
						name = "<no target>" 
					end
					msg = string.gsub( msg, "%%t", name )
				end
				-- replace %m <mouseover>
				if(string.find(msg, "%%m")) then
					local name = UnitName("mouseover")
					if name == nil then 
						name = "<no mouseover>"					
					end
					msg = string.gsub( msg, "%%m", name )
				end
				--replace %f <focus>
				if(string.find(msg, "%%f")) then
					local name = UnitName("focus")
					if name == nil then
						name = "<no focus>" 
					end
					msg = string.gsub( msg, "%%f", name )
				end
				--format text and replace color tags
				msg = Horizons.Tools:FormatText(msg)
				--encode links to make them clickable if option is checked
				if(Horizons.db.global.Settings.Channel.HyperlinkUrls) then
					msg = Horizons.Comms:EncodeURLs(msg)
				end
				if(Horizons.db.global.Settings.Channel.Locked) then 
					if(Horizons.Config:IsOfficer(UnitName("player"))) then
						Horizons.Comms:Send(channel, msg)
					elseif (channel == Horizons:GetPlayerClass("player") or channel == "HEAL" or channel == "TANK" or channel == "SHOUT") then
						Horizons.Comms:Send(channel, msg)
					else
						Horizons:Print("You tried to write in "..channel.." but channels are currently locked!")
					end
				else
					Horizons.Comms:Send(channel, msg)
				end
			else
				Horizons:Print("You tried to write in a chat that you cannot see!")
			end	
		else
			Horizons:Print("That is not a viable channel!")
		end
	end
end

--[[
	Sends a clickable message to the global chat channel.
]]
function Horizons.Comms:SendURL(url)
	if(type(url) == "string" and string.trim(url) ~= "") then
		Horizons.Comms:Send("SHOUT", Horizons.Comms.CreateURLLink(url))
	end
end

--[[
	Creates a clickable link.
]]
function Horizons.Comms.CreateURLLink(url)
	return "\124cff00FF00\124HCURL:" .. url .. "\124h[" .. url .. "]\124h\124r"
end

--[[
	Does something smart to detect and encode urls into clickable links.
]]
function Horizons.Comms:EncodeURLs(msg)
	if type(msg) == "string" then
		msg = string.gsub( msg, " www%.([_A-Za-z0-9-]+)%.(%S+)%s?",                                     Horizons.Comms.CreateURLLink("www.%1.%2") )
		msg = string.gsub( msg, " (%a+)://(%S+)%s?",                                                    Horizons.Comms.CreateURLLink("%1://%2"))
		msg = string.gsub( msg, " ([_A-Za-z0-9-%.]+)@([_A-Za-z0-9-]+)(%.+)([_A-Za-z0-9-%.]+)%s?",       Horizons.Comms.CreateURLLink("%1@%2%3%4") )
		msg = string.gsub( msg, " (%d%d?%d?)%.(%d%d?%d?)%.(%d%d?%d?)%.(%d%d?%d?):(%d%d?%d?%d?%d?)%s?",  Horizons.Comms.CreateURLLink("%1.%2.%3.%4:%5") )
		msg = string.gsub( msg, " (%d%d?%d?)%.(%d%d?%d?)%.(%d%d?%d?)%.(%d%d?%d?)%s?",                   Horizons.Comms.CreateURLLink("%1.%2.%3.%4") )
		msg = string.gsub( msg, " ([_A-Za-z0-9-]+)%.([_A-Za-z0-9-]+)%.(%S+)%s?",                        Horizons.Comms.CreateURLLink("%1.%2.%3") )
		msg = string.gsub( msg, " ([_A-Za-z0-9-]+)%.([_A-Za-z0-9-]+)%.(%S+)%:([_0-9-]+)%s?",            Horizons.Comms.CreateURLLink("%1.%2.%3:%4") )
	end
	return msg
end

--[[ 
	Sends a addon command to the chat channel.
]]
function Horizons.Comms:SendCommand(cmd)
	if(type(cmd) == "string") then
		Horizons.Comms:Send("COMMAND", cmd)
	end
end

--[[
	Sends a addon command through a addon whisper.
]]
function Horizons.Comms:SendAddonMessage(prefix, distribution, target, args)
	local cmd = prefix
	if ( args ~= nil ) then
		for i, v in pairs(args) do
			cmd = cmd .. Horizons.Comms.Separator .. v
		end
	end
	if(distribution == "GLOBAL") then
		Horizons.Comms:SendCommand(cmd)
	else
		Horizons:SendCommMessage("Horizons", cmd, distribution, target)
	end
end

--[[
	Processes an incoming message from the chat channel
]]
function Horizons.Comms:ProcessMessage(channel, message, sender)
	if(channel == "COMMAND") then
		Horizons.Comms:ProcessCommand(message, sender)
	else
		if(Horizons.Comms:IsViableChannel(channel) and Horizons.db.global.Settings.Channel[channel].Visible) then
			if(Horizons.db.global.Settings.Channel.Locked) then 
				if(Horizons.Config:IsOfficer(UnitName("player"))) then
					Horizons.Comms:Print(channel, message, sender)
				elseif (channel == Horizons:GetPlayerClass("player") or channel == "HEAL" or channel == "TANK" or channel == "SHOUT") then
					Horizons.Comms:Print(channel, message, sender)
				end
			else
				Horizons.Comms:Print(channel, message, sender)
			end
		end
	end
end

--[[
	Splits the incomming command into it's original form and sends it its handler.
]]
function Horizons.Comms:ProcessCommand(cmd, sender)
	local input = {strsplit(Horizons.Comms.Separator, cmd)}
	if(Horizons.Tools.CmdHandlers[input[1]]) then
		Horizons.Tools.CmdHandlers[input[1]](input, sender)
	end
end

--[[ 
	Prints a channel message to the set chatframe.
]]
function Horizons.Comms:Print(channel, msg, sender)
	local ChatFrame = Horizons.Config:GetChannelChatFrame(channel)
	ChatFrame:AddMessage( "[HZ"..Horizons.Comms.ABBREVIATIONS[channel].."]\124r\124Hplayer:"..sender.."\124h["..sender.."]\124h: " .. msg, Horizons.Config:GetChannelColor(string.upper(channel), sender) )
end

--[[
	Checks if a given channel exists in the system.
]]
function Horizons.Comms:IsViableChannel(channel)
	for i, v in pairs(Horizons.Comms.ChannelList) do
		if(v == string.upper(channel)) then
			return true
		end
	end
	return false
end

--[[
	Checks if the user is connected to the addon chat channel.
]]
function Horizons.Comms:IsJoined()
	return GetChannelName(Horizons.Comms.Name) ~= 0;
end

--[[
	Opens the url grabber window with a given text.
]]
function Horizons.Comms:ShowURL(url)
	if(not type(url) == "string") then
		return
	end
	local AceGUI = LibStub("AceGUI-3.0")
	local frame = AceGUI:Create("Window")
	frame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end)
	frame:SetTitle("UrlGrabber")
	frame:SetLayout("Fill")
	frame:SetWidth(400)
	frame:SetHeight(100)
	frame:EnableResize(false)
	local editbox = AceGUI:Create("EditBox")
	editbox:SetRelativeWidth(1)
	editbox:SetLabel("Select and Ctrl+C to copy")
	frame:AddChild(editbox)
	frame:Show()
	editbox.editbox:SetText(url)
	editbox.editbox:HighlightText();
	editbox.editbox:SetFocus();	
end

--[[
	Command handler for locking the chat channels.
]]
function Horizons.Comms.HandleLock(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		Horizons.db.global.Settings.Channel.Locked = true
		Horizons:Print(sender .. " locked the chat channels")
	end
end

--[[
	Command handler for unlocking the chat channels.
]]
function Horizons.Comms.HandleUnlock(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		Horizons.db.global.Settings.Channel.Locked = false
		Horizons:Print(sender .. " unlocked the chat channels")
	end
end

--[[
	Encrypts a message.
]]
function Horizons.Comms:Encrypt(msg)
	local b64 = "yz012LdefghSnoMNO67ABix8ETUVWXPQRCDGHIJKcpqrstuvwZab345YjklmF9/#"
    local ret_msg = ""
    local msg_length = strlen(msg)
    local msgloop
    if mod(msg_length,3) == 2 then
        msg = " "..msg
        msg_length = strlen(msg)
    end
    if mod(msg_length,3) == 1 then
        msg = "  "..msg
        msg_length = strlen(msg)
    end
    for msgloop = 1,msg_length,3 do

        local char1 = string.byte( msg, msgloop )
        local char2 = string.byte( msg, msgloop+1 )
        local char3 = string.byte( msg, msgloop+2 )

        local byte1 = floor(char1 / 4)                              
        local byte2 = (16 * mod(char1,4)) + floor(char2/16)         
        local byte3 = (4  * mod(char2,16)) + floor(char3/64)       
        local byte4 = mod( char3, 64 )                             

        local thisBlock = strsub(b64,byte1+1,byte1+1)..strsub(b64,byte2+1,byte2+1)..strsub(b64,byte3+1,byte3+1)..strsub(b64,byte4+1,byte4+1)
        
        ret_msg = ret_msg..thisBlock
    end
    return ret_msg 
end

--[[
	Decrypts a message.
]]
function Horizons.Comms:Decrypt(base64msg)
	local b64 = "yz012LdefghSnoMNO67ABix8ETUVWXPQRCDGHIJKcpqrstuvwZab345YjklmF9/#"
    local ret_msg = ""
    local msg_length = strlen(base64msg)
    local msgloop
    for msgloop = 1,msg_length,4 do
        local char1 = string.sub( base64msg, msgloop,   msgloop   )
        local char2 = string.sub( base64msg, msgloop+1, msgloop+1 )
        local char3 = string.sub( base64msg, msgloop+2, msgloop+2 )
        local char4 = string.sub( base64msg, msgloop+3, msgloop+3 )

        if (char1=="^") or (char1=="$") or (char1=="(") or (char1==")") or (char1=="%") or (char1==" ") or (char1=="*") or (char1=="+") or (char1=="-") or (char1=="?") then return ""; end
        if (char2=="^") or (char2=="$") or (char2=="(") or (char2==")") or (char2=="%") or (char2==" ") or (char2=="*") or (char2=="+") or (char2=="-") or (char2=="?") then return ""; end
        if (char3=="^") or (char3=="$") or (char3=="(") or (char3==")") or (char3=="%") or (char3==" ") or (char3=="*") or (char3=="+") or (char3=="-") or (char3=="?") then return ""; end
        if (char4=="^") or (char4=="$") or (char4=="(") or (char4==")") or (char4=="%") or (char4==" ") or (char4=="*") or (char4=="+") or (char4=="-") or (char4=="?") then return ""; end

        local byte1 = string.find( b64, char1 )
        local byte2 = string.find( b64, char2 )
        local byte3 = string.find( b64, char3 )
        local byte4 = string.find( b64, char4 )

        byte1 = byte1 - 1
        byte2 = byte2 - 1
        byte3 = byte3 - 1
        byte4 = byte4 - 1

        local decbyte1 = 4 * byte1 + floor(byte2/16)            
        local decbyte2 = 16 * mod(byte2,16) + floor(byte3/4)    
        local decbyte3 = 64 * mod(byte3,4)  + byte4             

        local thisBlock = string.char( decbyte1, decbyte2, decbyte3 )

        ret_msg = ret_msg..thisBlock
    end
    return ret_msg
end
