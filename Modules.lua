Horizons.Modules = {}
Horizons.Modules.ModuleList = {}

function Horizons.Modules:RegisterModule(Modulef)
	local m = Modulef()
	Horizons.Modules.ModuleList[m.Name] = m
end

function Horizons.Modules:ToggleModule(name)
	if(Horizons.db.global.Settings.Modules[name].Enabled) then
		Horizons.db.global.Settings.Modules[name].Enabled = false
		Horizons.Modules.ModuleList[name]:Disable()
	else
		Horizons.db.global.Settings.Modules[name].Enabled = true
		Horizons.Modules.ModuleList[name]:Enable()
	end
end

function Horizons.Modules:GetModuleState(name)
	return Horizons.db.global.Settings.Modules[name].Enabled
end

function Horizons.Modules:Initialise()
	local count = 0
	for k, v in pairs(Horizons.Modules.ModuleList) do
		if(Horizons.db.global.Settings.Modules[v.Name].Enabled) then
			v:Enable()
		end
		count = count + 1
	end
	Horizons:Print(count .. " Module(s) loaded")
end
