Horizons.Tools.DurabilityCheck = {}
Horizons.Tools.DurabilityCheck.SlotList = {["HeadSlot"]={}, ["ShoulderSlot"]={}, ["ChestSlot"]={}, ["WristSlot"]={}, ["HandsSlot"]={}, ["WaistSlot"]={}, ["LegsSlot"]={}, ["FeetSlot"]={}, ["MainHandSlot"]={}, ["SecondaryHandSlot"]={}}

function Horizons.Tools.DurabilityCheck:Init()
	Horizons.Tools:RegisterCmdHandler("DURCHECKREQ", Horizons.Tools.DurabilityCheck.HandleRequest)
	Horizons.Tools:RegisterCmdHandler("DURCHECKRES", Horizons.Tools.DurabilityCheck.HandleResponse)
	Horizons:RegisterChatCommand("durabilitycheck", Horizons.Tools.DurabilityCheck.PerformCheck)
	Horizons:RegisterChatCommand("durcheck", Horizons.Tools.DurabilityCheck.PerformCheck)
	Horizons:RegisterChatCommand("dcheck", Horizons.Tools.DurabilityCheck.PerformCheck)
end

function Horizons.Tools.DurabilityCheck.PerformCheck()
	local player = UnitName("player")
	local destination = nil
	if Horizons.WowApi.IsInParty() then
		destination = "PARTY"
	end
	if Horizons.WowApi.IsInRaid() then
		destination = "RAID"
	end
	if destination == nil then
		Horizons:Print("You are not in a group")
		return
	end
	if((Horizons.WowApi.IsGroupLeader() or Horizons.WowApi.IsGroupAssistant() or Horizons.Config:IsOfficer(player)) and destination ~= nil) then
		SendChatMessage("Performing durability check", destination)
		Horizons.Comms:SendAddonMessage("DURCHECKREQ", destination, nil, nil)
	end
end

function Horizons.Tools.DurabilityCheck.HandleRequest(input, sender)
	Horizons.Comms:SendAddonMessage("DURCHECKRES", "WHISPER", sender, {Horizons.Tools.DurabilityCheck.DurabilityCheck()})
end

function Horizons.Tools.DurabilityCheck.DurabilityCheck()
	local current
	local maximum
	local overall
	local count = 0
	local sum = 0
	local lowest = 100
	
	for k, v in pairs(Horizons.Tools.DurabilityCheck.SlotList) do
		current, maximum = GetInventoryItemDurability(GetInventorySlotInfo(k));
		if(type(current) == "number" and type(maximum) == "number") then
			Horizons.Tools.DurabilityCheck.SlotList[k].Percentage = math.floor(current / maximum * 100)
			sum = sum + Horizons.Tools.DurabilityCheck.SlotList[k].Percentage
			count = count + 1
			if(Horizons.Tools.DurabilityCheck.SlotList[k].Percentage < lowest) then
				lowest = Horizons.Tools.DurabilityCheck.SlotList[k].Percentage
			end
		end
	end
	overall = math.floor(sum / count)
	return lowest, overall
end

function Horizons.Tools.DurabilityCheck.HandleResponse(input, sender)
	local destination = nil
	if Horizons.WowApi.IsInParty() then
		destination = "PARTY"
	end
	if Horizons.WowApi.IsInRaid() then
		destination = "RAID"
	end
	if destination == nil then
		return
	end
	SendChatMessage(string.format("Got: %s, lowest = %s, average = %s", sender, input[2].."%", input[3].."%"), destination)
end

Horizons.Tools.DurabilityCheck:Init()
