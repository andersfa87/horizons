
function Horizons.Tools.HandleSlashAlert(input)
	if(input[1]) then		
		if(input[1] == "raid") then
			if(input[2]) then
				local str = table.concat(input, " ", 2)
				Horizons:SendCommMessage("Horizons", "ALERT"..Horizons.Comms.Separator .. str, "RAID")
			else
				Horizons:Print("Please specify a message")
			end
		elseif(input[1] == "global") then
			if(input[2]) then
				local str = table.concat(input, " ", 2)
				Horizons.Comms:SendCommand("ALERT"..Horizons.Comms.Separator .. str)
			else
				Horizons:Print("Please specify a message")
			end
		else
			Horizons:Print("Must be either global or raid")
		end
	else
		Horizons:Print("Please specify either global or raid")
	end
end

function Horizons.Tools.HandleAlert(input, sender)
	if(Horizons.Config:IsOfficer(sender)) then
		if(input[2]) then
			if(type(input[2]) == "string") then
				if(#input > 2) then
					for i=3, #input do
						input[2] = input[2] .. "�" .. input[i]
					end
				end
				Horizons.Tools:ShowAlert(input[2], sender)
			end
		end
	end
end

function Horizons.Tools:ShowAlert(text, sender)
	if(type(text) == "string" and type(sender) == "string") then
		if(not Horizons.Tools.AlertFrame) then
			Horizons.Tools.AlertFrame = CreateFrame("Frame", "HorizonsAlertFrame", UIParent)
			Horizons.Tools.AlertFrame:SetWidth(800);
			Horizons.Tools.AlertFrame:SetHeight(35);
			Horizons.Tools.AlertFrame:SetPoint("TOP", 0, -200)
			Horizons.Tools.AlertFrame:EnableMouse(false);
			Horizons.Tools.AlertFrame:SetFrameStrata("HIGH")
			Horizons.Tools.AlertFrame.Text = Horizons.Tools.AlertFrame:CreateFontString("HorizonsAlertText", "OVERLAY")
			Horizons.Tools.AlertFrame.Text:SetFont("Fonts\\FRIZQT__.TTF", 35)
			Horizons.Tools.AlertFrame.Text:SetTextColor(1*0.8,0,0,1)
			Horizons.Tools.AlertFrame.Text:SetShadowOffset(2, -2)
			Horizons.Tools.AlertFrame.Text:SetJustifyH("LEFT")
			Horizons.Tools.AlertFrame.Text:SetJustifyV("TOP")
			Horizons.Tools.AlertFrame.Text:SetHeight(35)
			Horizons.Tools.AlertFrame.Text:SetPoint("TOP")
			Horizons.Tools.AlertFrame.Text:SetNonSpaceWrap(false)
		end
		Horizons.Tools.AlertFrame.Text:SetText(text);
		Horizons.Tools.AlertFrame:Show()
		DEFAULT_CHAT_FRAME:AddMessage( Horizons.Tools:FormatText("<_WHITE_"..sender.."_END_-ALERT!> _WHITE_"..text), 1,0,0,1 )
		Horizons:ScheduleTimer(function() 	
			if(Horizons.Tools.AlertFrame) then
				Horizons.Tools.AlertFrame.Text:SetText("")
				Horizons.Tools.AlertFrame:Hide()
			end 
		end, 4)
	end
end