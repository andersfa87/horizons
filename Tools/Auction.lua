function Horizons.Tools.HandleSlashAuction(input)
	Horizons.Tools.CreateAuctionWindow()
end

function Horizons.Tools.CreateAuctionWindow()
	if not(Horizons.Tools.Auction) then Horizons.Tools.Auction = {} end
	
	if not(Horizons.Tools.Auction.Frame) then	
		--States
		--0 = reset
		--1 = bidding
		--2 = stopped
		local AceGUI = LibStub("AceGUI-3.0")
		Horizons.Tools.Auction.State = 0
		
		Horizons.Tools.Auction.Frame = AceGUI:Create("Window")
		Horizons.Tools.Auction.Frame:SetCallback("OnClose", function() Horizons.Tools.Auction.Frame:Hide() end)
		Horizons.Tools.Auction.Frame:SetTitle("Horizons Auction (0.3)")
		Horizons.Tools.Auction.Frame:SetLayout("List")
		Horizons.Tools.Auction.Frame:SetWidth(455)
		Horizons.Tools.Auction.Frame:SetHeight(350)
		Horizons.Tools.Auction.Frame:EnableResize(false)
		
		Horizons.Tools.Auction.Frame.Status = AceGUI:Create("Label")
		Horizons.Tools.Auction.Frame.Status:SetText("Current State : Reset")
		Horizons.Tools.Auction.Frame.Status:SetFont("Fonts\\FRIZQT__.TTF", 10)
		Horizons.Tools.Auction.Frame.Status:SetRelativeWidth(1)
		Horizons.Tools.Auction.Frame:AddChild(Horizons.Tools.Auction.Frame.Status)
				
		local btnGroup = AceGUI:Create("InlineGroup")
		btnGroup:SetRelativeWidth(1)
		btnGroup:SetLayout("Flow")
		btnGroup:SetTitle("")
		Horizons.Tools.Auction.Frame:AddChild(btnGroup)
		
		Horizons.Tools.Auction.Frame.StopBtn = AceGUI:Create("Button")
		Horizons.Tools.Auction.Frame.StopBtn:SetText("Click to stop")
		Horizons.Tools.Auction.Frame.StopBtn:SetDisabled(true)
		btnGroup:AddChild(Horizons.Tools.Auction.Frame.StopBtn)
		
		
		Horizons.Tools.Auction.Frame.ResetBtn = AceGUI:Create("Button")
		Horizons.Tools.Auction.Frame.ResetBtn:SetText("Click to Reset")
		btnGroup:AddChild(Horizons.Tools.Auction.Frame.ResetBtn)
		
		Horizons.Tools.Auction.Frame.PrintOfficerBtn = AceGUI:Create("Button")
		Horizons.Tools.Auction.Frame.PrintOfficerBtn:SetText("Post in Officer")
		Horizons.Tools.Auction.Frame.PrintOfficerBtn:SetDisabled(true)
		btnGroup:AddChild(Horizons.Tools.Auction.Frame.PrintOfficerBtn)
		
		Horizons.Tools.Auction.Frame.PrintRaidBtn = AceGUI:Create("Button")
		Horizons.Tools.Auction.Frame.PrintRaidBtn:SetText("Post in Raid")
		Horizons.Tools.Auction.Frame.PrintRaidBtn:SetDisabled(true)
		btnGroup:AddChild(Horizons.Tools.Auction.Frame.PrintRaidBtn)
		
		local lbl = AceGUI:Create("Label")
		lbl:SetText("-------------------------------------------------Bids-------------------------------------------------")
		lbl:SetFont("Fonts\\FRIZQT__.TTF", 10)
		lbl:SetRelativeWidth(1)
		Horizons.Tools.Auction.Frame:AddChild(lbl)
		
		AceGUI:RegisterWidgetType("CV_ScrollingMessageFrame", function()
			local frame = CreateFrame("Frame",nil,UIParent)
			local scrframe = CreateFrame("ScrollingMessageFrame",nil,frame)
			local self = {}
			self.type = "CV_ScrollingMessageFrame"
			
			self.OnRelease = function(self) end
			self.OnAcquire = function(self) end
			self.frame = frame
			frame.obj = self
			
			scrframe:SetFontObject("ChatFontNormal")
			scrframe:SetJustifyH("LEFT")
			scrframe:SetMaxLines(25)
			scrframe:SetHyperlinksEnabled(true)
			--changed in 4.0
			--scrframe:SetInsertMode("TOP" or "BOTTOM")
			scrframe:SetFading(false)
			scrframe:EnableMouse(true)
			scrframe:EnableMouseWheel(true)
			scrframe:SetPoint("TOPLEFT",frame,"TOPLEFT",0,-10)
			scrframe:SetPoint("BOTTOMRIGHT",frame,"BOTTOMRIGHT",0,10)
			scrframe:SetScript("OnMouseWheel", function(obj, arg)
													if(arg == 1) then 
														obj:ScrollUp();
													else
														obj:ScrollDown();
													end 
												end)
			scrframe:SetScript("OnHyperLinkClick", function(self, link, text, button) Horizons:SetItemRef(link, text, button) end)
			self.scrframe = scrframe
			
			frame:SetHeight(100)
			frame:SetWidth(100)		
			
			AceGUI:RegisterAsWidget(self)
			return self
		end, 1)
		
		Horizons.Tools.Auction.Frame.Bids = AceGUI:Create("CV_ScrollingMessageFrame")
		Horizons.Tools.Auction.Frame.Bids:SetRelativeWidth(1)
		Horizons.Tools.Auction.Frame.Bids:SetHeight(200)
		Horizons.Tools.Auction.Frame:AddChild(Horizons.Tools.Auction.Frame.Bids)
		
		
		Horizons.Tools.Auction.Frame.StopBtn:SetCallback("OnClick", function() 
											if(Horizons.Tools.Auction.State == 1) then
												Horizons.Tools.UpdateStatus(2)
												SendChatMessage("==============================", "RAID")
												SendChatMessage("Auction Stopped!", "RAID")
												SendChatMessage("==============================", "RAID")
											end
										end)
										
		Horizons.Tools.Auction.Frame.ResetBtn:SetCallback("OnClick", function()
												Horizons.Tools.UpdateStatus(0)
										end)
										
		Horizons.Tools.Auction.Frame.PrintOfficerBtn:SetCallback("OnClick", function()
											Horizons.Tools.PostBids("OFFICER")
										end)
		
		Horizons.Tools.Auction.Frame.PrintRaidBtn:SetCallback("OnClick", function()
											Horizons.Tools.PostBids("RAID")
										end)
			

		
	end
	Horizons.Tools.Auction.Frame:Show()
end

function Horizons.Tools.UpdateStatus(newState)
	if(type(newState) == "number") then
		Horizons.Tools.Auction.State = newState
		if    (Horizons.Tools.Auction.State == 0) then
			Horizons.Tools.Auction.Frame.ResetBtn:SetDisabled(false)
			Horizons.Tools.Auction.Frame.StopBtn:SetDisabled(true)
			Horizons.Tools.Auction.Frame.PrintOfficerBtn:SetDisabled(true)
			Horizons.Tools.Auction.Frame.PrintRaidBtn:SetDisabled(true)
			Horizons.Tools.Auction.Frame.Bids.scrframe:Clear()
			Horizons.Tools.Auction.Frame.Status:SetText("Current State : Reset")
		elseif(Horizons.Tools.Auction.State == 1) then
			Horizons.Tools.Auction.Frame.ResetBtn:SetDisabled(false)
			Horizons.Tools.Auction.Frame.StopBtn:SetDisabled(false)
			Horizons.Tools.Auction.Frame.PrintOfficerBtn:SetDisabled(true)
			Horizons.Tools.Auction.Frame.PrintRaidBtn:SetDisabled(true)
			Horizons.Tools.Auction.Frame.Status:SetText("Current State : Taking Bids")
		elseif(Horizons.Tools.Auction.State == 2) then
			Horizons.Tools.Auction.Frame.ResetBtn:SetDisabled(false)
			Horizons.Tools.Auction.Frame.StopBtn:SetDisabled(true)
			Horizons.Tools.Auction.Frame.PrintOfficerBtn:SetDisabled(false)
			Horizons.Tools.Auction.Frame.PrintRaidBtn:SetDisabled(false)
			Horizons.Tools.Auction.Frame.Status:SetText("Current State : Stopped")
		end
	end
end

function Horizons.Tools.NewItemForAuction(input)
	local player = UnitName("player")
	if(Horizons.Util.IsGroupLeader(player) or Horizons.Util.IsGroupAssistant(player) or Horizons.Config:IsOfficer(player)) then
		Horizons.Tools.CreateAuctionWindow()
		if not(Horizons.Tools.Auction.State == 1) then
			Horizons.Tools.UpdateStatus(1)
			
			Horizons.Tools.Auction.CurrentItem = input
			Horizons.Tools.Auction.Bids = {}
			Horizons.Tools.Auction.Frame.Bids.scrframe:Clear()
			
			local slotMap = {   
				["INVTYPE_HEAD"] = 1,
				["INVTYPE_NECK"] = 2,
				["INVTYPE_SHOULDER"] = 3,
				["INVTYPE_BODY"] = 4,
				["INVTYPE_ROBE"] = 5,
				["INVTYPE_CHEST"] = 5,
				["INVTYPE_WAIST"] = 6,
				["INVTYPE_LEGS"] = 7,
				["INVTYPE_FEET"] = 8,
				["INVTYPE_WRIST"] = 9,
				["INVTYPE_HAND"] = 10,
				["INVTYPE_FINGER"] = 11,
				["INVTYPE_TRINKET"] = 13,
				["INVTYPE_CLOAK"] = 15,
				["INVTYPE_WEAPON"] = 16,
				["INVTYPE_WEAPONMAINHAND"] = 16,
				["INVTYPE_WEAPONOFFHAND"] = 17,
				["INVTYPE_2HWEAPON"] = 16,
				["INVTYPE_SHIELD"] = 17,
				["INVTYPE_HOLDABLE"] = 17,
				["INVTYPE_THROWN"] = 18,
				["INVTYPE_RANGED"] = 18,
				["INVTYPE_RELIC"] = 18,
				["INVTYPE_RANGEDRIGHT"] = 18
			}
			local slotName = select(9, GetItemInfo(input))
			local slotId = 0
			if (slotMap[slotName]) then
				slotId = slotMap[slotName]
			end
			
			SendChatMessage("==============================", "RAID")
			SendChatMessage("New Auction! (" .. input .. ")", "RAID")
			SendChatMessage("==============================", "RAID")
			Horizons.Comms:SendAddonMessage("AUCTIONSTART", "RAID", nil, {slotId, input})
			
		else
			Horizons:Print("End the current auction before you start a new please")
		end
	else
		Horizons:Print("You are not allowed to do that")
	end
end

function Horizons.Tools.HandleBid(input, sender)
	if(Horizons.Tools.Auction) then
		if(Horizons.Tools.Auction.State == 1) then
			if not(Horizons.Tools.Auction.Bids[sender]) then
				if (input[2]) then
						
						Horizons.Tools.Auction.Bids[sender] = {spec = "(Main)", citem = input[2], ilvl = ""}
						if(input[3]) then
							if(input[3] == "OFF") then
								Horizons.Tools.Auction.Bids[sender].spec = "(Off)"
							end
						end
						
						if(input[4]) then
							Horizons.Tools.Auction.Bids[sender].ilvl = " - " .. input[4]
						end
						
						Horizons.Tools.Auction.Frame.Bids.scrframe:AddMessage(sender .. " " .. Horizons.Tools.Auction.Bids[sender].spec .. Horizons.Tools.Auction.Bids[sender].ilvl .. "\n         " .. Horizons.Tools.Auction.Bids[sender].citem, 1, 1, 1, nil, false)
						
						Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {"Your bid has been registered"})
				end
			else
				Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {"Your have already bidded on this item!"})
			end
		else
			Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {"Auction is closed!"})
		end
	end
end

function Horizons.Tools.PostBids(dest)
	SendChatMessage("Item: " .. Horizons.Tools.Auction.CurrentItem, dest)
	SendChatMessage("==============================", dest)
	local counter = 0
	for k, v in pairs(Horizons.Tools.Auction.Bids) do
		SendChatMessage(k .. " " .. v.spec .. v.ilvl .. " " .. v.citem, dest)
		counter = counter + 1
	end
	if(counter == 0) then
		SendChatMessage("No bids!", dest)
	end
	SendChatMessage("==============================", dest)
end


function Horizons.Tools.BidOnAuction(link)
	if(link) then
		local info = {strsplit(":", link)}
		if(#info == 2) then
			--svar
			--1 = id
			--2 = sender
			info[1] = tonumber(info[1])
			if(type(info[1]) == "number") then
				local itemLink = "Itemslot not found!"
				if not(info[1] == 0) then
					itemLink = GetInventoryItemLink("player", info[1])
					if(info[1] == 11 or info[1] == 13) then
						local itemLink2 = GetInventoryItemLink("player", info[1]+1)
						itemLink = itemLink .. " + " .. itemLink2
					end
					if(itemLink == nil) then
						itemLink = "No item equipped in the specified slot";
					end
				end
				
				local ilvlavg = select(2, Horizons.Tools:GearCheck("player"))
			
				local AceGUI = LibStub("AceGUI-3.0")
				local frame = AceGUI:Create("Window")
				frame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end)
				frame:SetTitle("Bidding Window")
				frame:SetLayout("List")
				frame:SetWidth(200)
				frame:SetHeight(100)
				frame:EnableResize(false)
				
				local MainBtn = AceGUI:Create("Button")
				MainBtn:SetText("Main Spec")
				MainBtn:SetRelativeWidth(1)
				MainBtn:SetCallback("OnClick", function() Horizons.Comms:SendAddonMessage("HANDLEBID", "WHISPER", info[2], {itemLink, "MAIN", ilvlavg}) AceGUI:Release(frame) end)
				frame:AddChild(MainBtn)
				
				local OffBtn = AceGUI:Create("Button")
				OffBtn:SetText("Off Spec")
				OffBtn:SetRelativeWidth(1)
				OffBtn:SetCallback("OnClick", function() Horizons.Comms:SendAddonMessage("HANDLEBID", "WHISPER", info[2], {itemLink, "OFF", ilvlavg}) AceGUI:Release(frame) end)
				frame:AddChild(OffBtn)	
				
				frame:Show()
			end
		end
	end
end