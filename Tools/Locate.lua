--[[
	Handles the locate slash command
]]
function Horizons.Tools.HandleSlashLocate(input)
	if(input) then
		if(#input > 2) then
			Horizons:Print("Looking for exact or partial matches for '" .. input .. "'")
			Horizons.Comms:SendAddonMessage("LOCATE", "GLOBAL", nil, {input})
		else
			Horizons:Print("Your search phrase must be atleast 3 characters long")
		end
	else
		Horizons:Print("You need to specify a playername for the locate")
	end
end


--[[
	Handles the locate request
]]
function Horizons.Tools.HandleLocate(input, sender)
	if(input[2] and string.len(input[2]) > 2) then
		local temp = {}
		for _, value in pairs(Horizons.db.global.Settings.PlayerInfo.Characters) do
			for i = 1, string.len(value) do
				if(string.lower(string.sub(value, i, i+string.len(input[2])-1)) == string.lower(input[2])) then
					table.insert(temp, value)
				end
			end
		end
		if ( #temp > 0 ) then
			Horizons.Comms:SendAddonMessage("LOCATERESPONSE", "WHISPER", sender, temp)
		end
	end
end

--[[
	Handles a locateresponse
]]
function Horizons.Tools.HandleLocateResponse(input, sender)
	if(#input > 1) then
		for i = 2, #input do
			Horizons:Print("Found " .. input[i] .. ", currently playing " .. Horizons.Util.CreateCharacterLink(sender))
		end
	end
end