Horizons.IconAssignments = {
	IconList = {
		"{star}",
		"{circle}",
		"{diamond}",
		"{triangle}",
		"{moon}",
		"{square}",
		"{cross}",
		"{skull}"
	}
}

--[[

]]
function Horizons.IconAssignments.HandleSlashCommand(input)
	Horizons.IconAssignments.CreateFrame()
end

--[[

]]
function Horizons.IconAssignments.CreateFrame()
	if not (Horizons.IconAssignments.Frame) then
		local AceGUI = LibStub("AceGUI-3.0")
		Horizons.IconAssignments.Frame = AceGUI:Create("Window")
		Horizons.IconAssignments.Frame:SetCallback("OnClose", function() Horizons.IconAssignments.Frame:Hide() end)
		Horizons.IconAssignments.Frame:SetTitle("Horizons Icon Assignments (0.2)")
		Horizons.IconAssignments.Frame:SetLayout("List")
		Horizons.IconAssignments.Frame:SetWidth(110)
		Horizons.IconAssignments.Frame:SetHeight(250)
		Horizons.IconAssignments.Frame:EnableResize(false)
		Horizons.IconAssignments.Rows = {}
		for i = 1, 8 do
			local icon = UnitPopupButtons["RAID_TARGET_"..i]
			Horizons.IconAssignments.Rows[i] = {}
			Horizons.IconAssignments.Rows[i].Target = ""
			Horizons.IconAssignments.Rows[i].Group = AceGUI:Create("SimpleGroup")
			Horizons.IconAssignments.Rows[i].Group:SetLayout("Flow")
			Horizons.IconAssignments.Rows[i].Icon = AceGUI:Create("Icon")
			Horizons.IconAssignments.Rows[i].Icon:SetImage(icon.icon, icon.tCoordLeft,icon.tCoordRight,icon.tCoordTop,icon.tCoordBottom)
			Horizons.IconAssignments.Rows[i].Icon:SetImageSize(16,16)
			Horizons.IconAssignments.Rows[i].Icon:SetHeight(16)
			Horizons.IconAssignments.Rows[i].Icon:SetWidth(16)
			Horizons.IconAssignments.Rows[i].Icon:SetCallback("OnClick", 	function() 
				if (Horizons.Config:IsOfficer(UnitName("player")) or Horizons.WowApi.IsGroupLeader() or Horizons.WowApi.IsGroupAssistant()) then
					if (IsShiftKeyDown()) then
						local target = UnitName("target")
						if(Horizons.WowApi.IsInRaid(target) or Horizons.WowApi.IsInParty(target)) then
							Horizons.IconAssignments.SetIconTarget(i, target)
						else
							SetRaidTarget( "target", i )
						end
					elseif (IsAltKeyDown()) then
						Horizons.IconAssignments.SetIconTarget(i, "")
					end
				end
			end)
			Horizons.IconAssignments.Rows[i].Label = AceGUI:Create("Label")			
			Horizons.IconAssignments.Rows[i].Group:AddChild(Horizons.IconAssignments.Rows[i].Icon)
			Horizons.IconAssignments.Rows[i].Group:AddChild(Horizons.IconAssignments.Rows[i].Label)
			Horizons.IconAssignments.Frame:AddChild(Horizons.IconAssignments.Rows[i].Group)
		end
		Horizons.IconAssignments.ButtonGroup = AceGUI:Create("SimpleGroup")
		Horizons.IconAssignments.ButtonGroup:SetLayout("List")
		--sync button
		Horizons.IconAssignments.SyncButton = AceGUI:Create("Button")
		Horizons.IconAssignments.SyncButton:SetText("Sync")
		Horizons.IconAssignments.SyncButton:SetWidth(90)
		Horizons.IconAssignments.SyncButton:SetCallback("OnClick", Horizons.IconAssignments.SynchronizeAssignments)
		Horizons.IconAssignments.ButtonGroup:AddChild(Horizons.IconAssignments.SyncButton)
		--broadcast button
		Horizons.IconAssignments.BroadcastButton = AceGUI:Create("Button")
		Horizons.IconAssignments.BroadcastButton:SetText("Broadcast")
		Horizons.IconAssignments.BroadcastButton:SetWidth(90)
		Horizons.IconAssignments.BroadcastButton:SetCallback("OnClick", Horizons.IconAssignments.BroadcastAssignments)
		Horizons.IconAssignments.ButtonGroup:AddChild(Horizons.IconAssignments.BroadcastButton)
		Horizons.IconAssignments.Frame:AddChild(Horizons.IconAssignments.ButtonGroup)
	end
	if not (Horizons.Config:IsOfficer(UnitName("player")) or Horizons.WowApi.IsGroupLeader() or Horizons.WowApi.IsGroupAssistant()) then
		Horizons.IconAssignments.SyncButton:SetDisabled(true)
	else
		Horizons.IconAssignments.SyncButton:SetDisabled(false)
	end
	Horizons.IconAssignments.Frame:Show()
end

--[[

]]
function Horizons.IconAssignments.SetIconTarget(index, name)
	if(name ~= nil) then
		Horizons.IconAssignments.Rows[index].Target = name
	else
		Horizons.IconAssignments.Rows[index].Target = ""
	end
	Horizons.IconAssignments.Rows[index].Label:SetText(Horizons.IconAssignments.Rows[index].Target)
end

--[[

]]
function Horizons.IconAssignments.SynchronizeAssignments()
	local temp = {}
	for i = 1, 8 do
		table.insert(temp, Horizons.IconAssignments.Rows[i].Target)
	end
	Horizons.Comms:SendAddonMessage("SYNCICONASSIGNMENTS", "RAID", nil, temp)
end

--[[

]]
function Horizons.IconAssignments.BroadcastAssignments()
	for i = 1, 8 do
		if (Horizons.IconAssignments.Rows[i].Target ~= "") then
			local line = Horizons.IconAssignments.IconList[i] .. " - " .. Horizons.IconAssignments.Rows[i].Target
			SendChatMessage(line, "RAID")
		end
	end
end

--[[

]]
function Horizons.IconAssignments.HandleInput(input, sender)
	if(#input == 9) and (Horizons.Config:IsOfficer(sender)) then
		if(Horizons.IconAssignments.Frame) then
			for i = 2, 9 do
				Horizons.IconAssignments.SetIconTarget(i-1, input[i])
			end
			Horizons:Print("Synchronized Icon Assignments (With " .. sender .. ")")
		end
	end
end