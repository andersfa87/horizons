function Horizons.Tools.ProfessionCheck(prof)
	if (not Horizons.Tools.TradeSkillList) then Horizons.Tools.CreateTradeSkillList() end
	if ( prof == "" ) then Horizons.Tools.CreateProfessionWindow() end
	if not(Horizons.Tools.TradeSkillList[string.lower(prof)]) then return end
	Horizons.Comms:SendAddonMessage("PROFESSION", "GLOBAL", nil, {prof})
	Horizons:Print("Looking for '" .. prof .. "'")
end

function Horizons.Tools.CreateProfessionWindow()
	if not(Horizons.Tools.ProfessionWindow) then
		local AceGUI = LibStub("AceGUI-3.0")
		local frame = AceGUI:Create("Window")
		frame:SetCallback("OnClose", function(widget) end)
		frame:SetTitle("Profession Check Window")
		frame:SetLayout("Fill")
		frame:SetWidth(400)
		frame:SetHeight(300)

		local scroll = AceGUI:Create("ScrollFrame")
		scroll:SetLayout("List")
		frame:AddChild(scroll)
		--tilf�je alle professions fra listen
		for k, v in pairs(Horizons.Tools.TradeSkillList) do
			local Abtn = AceGUI:Create("Button")
			Abtn:SetText(k)
			Abtn:SetRelativeWidth(1)
			Abtn:SetCallback("OnClick", function() Horizons.Tools.ProfessionCheck(k) frame:Hide() end)
			scroll:AddChild(Abtn)
		end
		frame:EnableResize(false)
		Horizons.Tools.ProfessionWindow = frame
	end
	Horizons.Tools.ProfessionWindow:Show()
end

function Horizons.Tools.HandleProfessionCheck(input, sender)
	if(input[2]) then
		local prof = string.lower(input[2])
		if (not Horizons.Tools.TradeSkillList) then Horizons.Tools.CreateTradeSkillList() end
		if not(Horizons.Tools.TradeSkillList[prof]) then return end
				
		local profs = { GetProfessions() }
		for i = 1, table.getn(profs) do
			if( profs[i] ~= nil ) then
				local name, _, skillLevel, maxSkillLevel, _, _, _, _ = GetProfessionInfo(profs[i])
				if( name == Horizons.Tools.TradeSkillList[prof] ) then
					local link, tradeskill_link = GetSpellLink(prof)
					local msg = " (" .. skillLevel .. "/" .. maxSkillLevel .. ")"
					if(tradeskill_link == nil) then
						if(link == nil) then
							msg = prof .. msg
						else
							msg = link .. msg
						end
					else
						msg = tradeskill_link .. msg
					end
					Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {msg})
				end
			end
		end
	end
end

function Horizons.Tools.CreateTradeSkillList()
	if (not Horizons.Tools.TradeSkillList) then
		Horizons.Tools.TradeSkillList = {
		["alchemy"] = "Alchemy",
		["blacksmithing"] = "Blacksmithing",
		["enchanting"] = "Enchanting",
		["engineering"] = "Engineering",
		["inscription"] = "Inscription",
		["jewelcrafting"] = "Jewelcrafting",
		["leatherworking"] = "Leatherworking",
		["tailoring"] = "Tailoring"
		}
	end
end