Horizons.Officer = {}

function Horizons.Officer:HandleSlashOfficer(input)
	if(Horizons.Config:IsOfficer(UnitName("player"))) then
		input = {strsplit(" ", input)}
		if(input[1] == "lock") then
			Horizons.Officer:LockChannels()
		elseif(input[1] == "unlock") then
			Horizons.Officer:UnlockChannels()
		elseif(input[1] == "timer") then
			if(input[2]) then
				input[2] = tonumber(input[2])
				if(type(input[2]) == "number") then 
					Horizons:SendCommMessage("Horizons", "TIMER"..Horizons.Comms.Separator .. input[2], "RAID")
				else
					Horizons:Print("Please specify a NUMBER duration for the timer")
				end
			else
				Horizons:Print("Please specify a duration for the timer")
			end
		elseif(input[1] == "version") then
			local cmd = {Horizons.Version}
			if(type(input[2]) == "string") then
				tinsert(cmd, input[2])
			end
			Horizons.Comms:SendAddonMessage("VERSION", "GLOBAL", nil, cmd)
		elseif(input[1] == "sound") then
			if(input[2]) then		
				if(input[2] == "raid") then
					if(input[3]) then
						input[3] = tonumber(input[3])
						if(type(input[3]) == "number") then
							Horizons:SendCommMessage("Horizons", "SOUND"..Horizons.Comms.Separator .. input[3], "RAID")
						else
							Horizons:Print("Soundfile must be a number (1-6)")
						end
					else
						Horizons:Print("Soundfile must be a number (1-6)")
					end
				elseif(input[2] == "global") then
					if(input[3]) then
						input[3] = tonumber(input[3])
						if(type(input[3]) == "number") then
							Horizons.Comms:SendCommand("SOUND"..Horizons.Comms.Separator .. input[3])
						else
							Horizons:Print("Soundfile must be a number (1-6)")
						end
					else
						Horizons:Print("Soundfile must be a number (1-6)")
					end
				else
					Horizons:Print("Must be either global or raid")
				end
			else
				Horizons:Print("Please specify either global or raid")
			end
		elseif(input[1] == "alert" or input[1] == "a") then
			table.remove(input, 1)
			Horizons.Tools.HandleSlashAlert(input)
		elseif(input[1] == "lookup") then
			
			if(type(input[2]) ~= "string") then Horizons:Print("Please specify a target") return end
			if(type(input[3]) ~= "string" and input[3] ~= "main") then Horizons:Print("Available lookup atm is 'main'") return end
			local cmd = {"LOOKUP"}
			tinsert(cmd, string.upper(input[3]))
			Horizons.Comms:SendAddonMessage("LOOKUP", "WHISPER", input[2], {"MAIN"})
		elseif(input[1] == "vote") then
			Horizons.Officer.CreateVoteWindow()
		elseif(input[1] == "auction") then
			table.remove(input, 1)
			Horizons.Tools.HandleSlashAuction(input)
		else
			Horizons:Print("Available commands for /hzo")
			Horizons:Print("  lock : Locks the chat channels")
			Horizons:Print("  unluck : Unlocks the chat channels")
			Horizons:Print("  timer [seconds] : Creates a timer with specified seconds duration")
			Horizons:Print("  version [link(optional)] : Performs a version check with optional link for download location")
			Horizons:Print("  sound [raid/global] [soundindex] : Plays a specific sound to either raid or global")
			Horizons:Print("  alert [raid/global] [msg] : Create a popup alert in either raid or global")
			Horizons:Print("  lookup [charname] [main\*] : Lookup a specific characters main")
			Horizons:Print("  vote : Opens the vote window")
			Horizons:Print("  auction : Opens the auction window")
		end
	end
end

function Horizons.Officer:LockChannels()
	if(Horizons.Config:IsOfficer(UnitName("player"))) then
		Horizons.Comms:SendCommand("LOCKCHANNELS")
	end
end

function Horizons.Officer:UnlockChannels()
	if(Horizons.Config:IsOfficer(UnitName("player"))) then
		Horizons.Comms:SendCommand("UNLOCKCHANNELS")
	end
end

function Horizons.Officer.LoadOfficerStuff()
	Horizons.Tools:RegisterCmdHandler("VOTEANSWER", Horizons.Officer.HandleVote)
end

function Horizons.Officer.CreateVoteWindow()
	if not(Horizons.Officer.Vote) then Horizons.Officer.Vote = {} end
	
	if not(Horizons.Officer.Vote.Frame) then
		local AceGUI = LibStub("AceGUI-3.0")
		Horizons.Officer.Vote.State = 0
		Horizons.Officer.Vote.Question = ""
		Horizons.Officer.Vote.Answers = {}
		Horizons.Officer.Vote.Votees = {}
		Horizons.Officer.Vote.TotalVotes = 0
		
		Horizons.Officer.Vote.Frame = AceGUI:Create("Window")
		Horizons.Officer.Vote.Frame:SetCallback("OnClose", function() Horizons.Officer.Vote.Frame:Hide() end)
		Horizons.Officer.Vote.Frame:SetTitle("Horizons Vote (0.3)")
		Horizons.Officer.Vote.Frame:SetLayout("List")
		Horizons.Officer.Vote.Frame:SetWidth(455)
		Horizons.Officer.Vote.Frame:SetHeight(370)
		Horizons.Officer.Vote.Frame:EnableResize(false)
		
		Horizons.Officer.Vote.Frame.Status = AceGUI:Create("Label")
		Horizons.Officer.Vote.Frame.Status:SetText("Current State : Reset")
		Horizons.Officer.Vote.Frame.Status:SetFont("Fonts\\FRIZQT__.TTF", 10)
		Horizons.Officer.Vote.Frame.Status:SetRelativeWidth(1)
		Horizons.Officer.Vote.Frame:AddChild(Horizons.Officer.Vote.Frame.Status)
		
		local btnGroup = AceGUI:Create("InlineGroup")
		btnGroup:SetRelativeWidth(1)
		btnGroup:SetLayout("Flow")
		Horizons.Officer.Vote.Frame:AddChild(btnGroup)
		
		Horizons.Officer.Vote.Frame.StopBtn = AceGUI:Create("Button")
		Horizons.Officer.Vote.Frame.StopBtn:SetText("Click to stop")
		Horizons.Officer.Vote.Frame.StopBtn:SetDisabled(true)
		btnGroup:AddChild(Horizons.Officer.Vote.Frame.StopBtn)
		
		
		Horizons.Officer.Vote.Frame.ResetBtn = AceGUI:Create("Button")
		Horizons.Officer.Vote.Frame.ResetBtn:SetText("Click to Reset")
		btnGroup:AddChild(Horizons.Officer.Vote.Frame.ResetBtn)
		
		Horizons.Officer.Vote.Frame.PrintOfficerBtn = AceGUI:Create("Button")
		Horizons.Officer.Vote.Frame.PrintOfficerBtn:SetText("Post in Officer")
		Horizons.Officer.Vote.Frame.PrintOfficerBtn:SetDisabled(true)
		btnGroup:AddChild(Horizons.Officer.Vote.Frame.PrintOfficerBtn)
		
		Horizons.Officer.Vote.Frame.PrintRaidBtn = AceGUI:Create("Button")
		Horizons.Officer.Vote.Frame.PrintRaidBtn:SetText("Post in Raid")
		Horizons.Officer.Vote.Frame.PrintRaidBtn:SetDisabled(true)
		btnGroup:AddChild(Horizons.Officer.Vote.Frame.PrintRaidBtn)
		
		Horizons.Officer.Vote.Frame.EditBoxQ = AceGUI:Create("EditBox")
		Horizons.Officer.Vote.Frame.EditBoxQ:SetLabel("Add your Question here, confirm with Enter press.")
		Horizons.Officer.Vote.Frame.EditBoxQ:SetText("")
		Horizons.Officer.Vote.Frame.EditBoxQ:SetRelativeWidth(1)
		Horizons.Officer.Vote.Frame.EditBoxQ:SetDisabled(false)
		Horizons.Officer.Vote.Frame.EditBoxQ:SetCallback("OnEnterPressed", function(widget, event, text) 
																Horizons.Officer.Vote.Question = text
															end)
		Horizons.Officer.Vote.Frame:AddChild(Horizons.Officer.Vote.Frame.EditBoxQ)
		
		Horizons.Officer.Vote.Frame.EditBoxA = AceGUI:Create("EditBox")
		Horizons.Officer.Vote.Frame.EditBoxA:SetLabel("Add your Answers here, add them to the list with Enter press.")
		Horizons.Officer.Vote.Frame.EditBoxA:SetText("")
		Horizons.Officer.Vote.Frame.EditBoxA:SetRelativeWidth(1)
		Horizons.Officer.Vote.Frame.EditBoxA:SetDisabled(false)
		Horizons.Officer.Vote.Frame:AddChild(Horizons.Officer.Vote.Frame.EditBoxA)
		
		Horizons.Officer.Vote.Frame.DropdownAnswers = AceGUI:Create("Dropdown")
		Horizons.Officer.Vote.Frame.DropdownAnswers:SetRelativeWidth(1)
		Horizons.Officer.Vote.Frame.DropdownAnswers:SetText("Click here to view your answers, select one to delete it.")
		Horizons.Officer.Vote.Frame.DropdownAnswers:SetMultiselect(false)
		Horizons.Officer.Vote.Frame.DropdownAnswers:SetList(nil)
		Horizons.Officer.Vote.Frame:AddChild(Horizons.Officer.Vote.Frame.DropdownAnswers)
				
		--Answers editbox on enter.
		Horizons.Officer.Vote.Frame.EditBoxA:SetCallback("OnEnterPressed", function(widget, event, text) 
																if(text == "") then return end
																tinsert(Horizons.Officer.Vote.Answers, {numVotes = 0, answer = text})
																local t = {}
																for i, v in pairs(Horizons.Officer.Vote.Answers) do
																	t[i] = v.answer
																end
																Horizons.Officer.Vote.Frame.DropdownAnswers:SetList(t)
																Horizons.Officer.Vote.Frame.EditBoxA:SetText("")
															end)
		Horizons.Officer.Vote.Frame.DropdownAnswers:SetCallback("OnValueChanged", function(widget, event, key, checked)
																		if(Horizons.Officer.Vote.State == 0) then
																			if(type(key) ~= "number") then return end
																			
																			tremove(Horizons.Officer.Vote.Answers, key)
																			local t = {}
																			for i, v in pairs(Horizons.Officer.Vote.Answers) do
																				t[i] = v.answer
																			end
																			Horizons.Officer.Vote.Frame.DropdownAnswers:SetList(t)
																		end
																		Horizons.Officer.Vote.Frame.DropdownAnswers:SetValue(nil)
																		Horizons.Officer.Vote.Frame.DropdownAnswers:SetText("Click here to view your answers, select one to delete it.")
																	end)
		
		
		
		
		Horizons.Officer.Vote.Frame.StartBtn = AceGUI:Create("Button")
		Horizons.Officer.Vote.Frame.StartBtn:SetText("START!")
		Horizons.Officer.Vote.Frame.StartBtn:SetDisabled(false)
		Horizons.Officer.Vote.Frame.StartBtn:SetRelativeWidth(1)
		Horizons.Officer.Vote.Frame:AddChild(Horizons.Officer.Vote.Frame.StartBtn)
		
		Horizons.Officer.Vote.Frame.StopBtn:SetCallback("OnClick", function() 
											if(Horizons.Officer.Vote.State == 1) then
												Horizons.Officer.UpdateVoteStatus(2)
											end
										end)
										
		Horizons.Officer.Vote.Frame.ResetBtn:SetCallback("OnClick", function()
												Horizons.Officer.UpdateVoteStatus(0)
												Horizons.Officer.Vote.Frame.DropdownAnswers:SetList(nil)
												Horizons.Officer.Vote.Question = ""
												Horizons.Officer.Vote.Answers = {}
												Horizons.Officer.Vote.Frame.EditBoxA:SetText("")
												Horizons.Officer.Vote.Frame.EditBoxQ:SetText("")
										end)
										
		Horizons.Officer.Vote.Frame.PrintOfficerBtn:SetCallback("OnClick", function()
											Horizons.Officer.PostVote("OFFICER")
										end)
		
		Horizons.Officer.Vote.Frame.PrintRaidBtn:SetCallback("OnClick", function()
											Horizons.Officer.PostVote("RAID")
										end)
										
		Horizons.Officer.Vote.Frame.StartBtn:SetCallback("OnClick", function()
											--TODO
											Horizons.Officer.StartVote()
										end)
	end
	Horizons.Officer.Vote.Frame:Show()
end

function Horizons.Officer.UpdateVoteStatus(newState)
	Horizons.Officer.Vote.State = newState
	if(Horizons.Officer.Vote.State == 0) then
		Horizons.Officer.Vote.Frame.StartBtn:SetDisabled(false)
		Horizons.Officer.Vote.Frame.PrintOfficerBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.PrintRaidBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.StopBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.EditBoxQ:SetDisabled(false)
		Horizons.Officer.Vote.Frame.EditBoxA:SetDisabled(false)
		Horizons.Officer.Vote.Frame.Status:SetText("Current State : Reset")
	elseif(Horizons.Officer.Vote.State == 1) then
		Horizons.Officer.Vote.Frame.StartBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.PrintOfficerBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.PrintRaidBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.StopBtn:SetDisabled(false)
		Horizons.Officer.Vote.Frame.EditBoxQ:SetDisabled(true)
		Horizons.Officer.Vote.Frame.EditBoxA:SetDisabled(true)
		Horizons.Officer.Vote.Frame.Status:SetText("Current State : Voting ("..Horizons.Officer.Vote.TotalVotes.." votes)")
	elseif(Horizons.Officer.Vote.State == 2) then
		Horizons.Officer.Vote.Frame.StartBtn:SetDisabled(false)
		Horizons.Officer.Vote.Frame.PrintOfficerBtn:SetDisabled(false)
		Horizons.Officer.Vote.Frame.PrintRaidBtn:SetDisabled(false)
		Horizons.Officer.Vote.Frame.StopBtn:SetDisabled(true)
		Horizons.Officer.Vote.Frame.EditBoxQ:SetDisabled(false)
		Horizons.Officer.Vote.Frame.EditBoxA:SetDisabled(false)
		Horizons.Officer.Vote.Frame.Status:SetText("Current State : Stopped ("..Horizons.Officer.Vote.TotalVotes.." votes)")
	end
end

function Horizons.Officer.StartVote(doGlobal)
	if(Horizons.Config:IsOfficer(UnitName("player"))) then
		if(Horizons.Officer.Vote.State ~= 1) then
			if(Horizons.Officer.Vote.Question ~= "" and #Horizons.Officer.Vote.Answers > 1) then
				Horizons.Officer.Vote.Votees = {}
				Horizons.Officer.Vote.TotalVotes = 0
				
				temp = {}
				table.insert(temp, "Q:"..Horizons.Officer.Vote.Question)
				
				for i = 1, #Horizons.Officer.Vote.Answers do
					Horizons.Officer.Vote.Answers[i].numVotes = 0
					table.insert(temp, ("A:"..Horizons.Officer.Vote.Answers[i].answer))
				end
				
				if(doGlobal) then
					Horizons.Comms:SendAddonMessage("VOTESTART", "GLOBAL", nil, temp)
				else
					Horizons.Comms:SendAddonMessage("VOTESTART", "RAID", nil, temp)
				end
				Horizons.Officer.UpdateVoteStatus(1)
			end
		end
	end
end

function Horizons.Officer.HandleVote(input, sender)
	if(Horizons.Officer.Vote) then
		if(Horizons.Officer.Vote.State == 1) then
		
			if( not Horizons.Officer.Vote.Votees[sender]) then
				Horizons.Officer.Vote.Votees[sender] = true
				local id = tonumber(input[2])
				if(type(id) == "number") then
					Horizons.Officer.Vote.Answers[id].numVotes = Horizons.Officer.Vote.Answers[id].numVotes+1
					Horizons.Comms:SendAddonMessage("MESSAGE", "WHISPER", sender, {"Your vote has been registered"})
				end
				Horizons.Officer.Vote.TotalVotes = Horizons.Officer.Vote.TotalVotes + 1
				Horizons.Officer.Vote.Frame.Status:SetText("Current State : Voting ("..Horizons.Officer.Vote.TotalVotes.." votes)")
			else
				--send deny m�ske?
				return
			end
		end
	end
end

function Horizons.Officer.PostVote(dest)
	if(Horizons.Officer.Vote.State == 2) then
		
		SendChatMessage("============VOTE RESULTS============", dest)
		SendChatMessage("====> " .. Horizons.Officer.Vote.Question, dest)
		SendChatMessage("====> " .. Horizons.Officer.Vote.TotalVotes .. " out of " .. Horizons.WowApi.GetNumGroupMembers() .. " voted", dest)
		SendChatMessage("=====================================", dest)
		for i, v in pairs(Horizons.Officer.Vote.Answers) do
			SendChatMessage("> ".. v.answer .. " ( " .. v.numVotes .. " )", dest)
		end
		SendChatMessage("=====================================", dest)
	end
end


