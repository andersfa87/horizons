Horizons.Config = {}

function Horizons.Config:InitialiseConfigGui()
	local options = {
		name = "Horizons",
		handler = Horizons,
		type = "group",
		args = {
			channels = {
				type = "group",
				name = "Channel Config",
				args = {
					header = {
						type = "header",
						name = "Channel Config",
						order = 1,
					},
					general = {
						type = "group",
						name = "General Chat Settings",
						inline = true,
						order = 2,
						args = {
							toggle = {
								type = "toggle",
								name = "Make URL's clickable",
								desc = "Click this to toggle making URL's clickable",
								width = "full",
								set = function(info, val) Horizons.db.global.Settings.Channel.HyperlinkUrls = val end,
								get = function() return Horizons.db.global.Settings.Channel.HyperlinkUrls end
							}
						}
					},
					shout = {
						type = "group",
						name = "Global Chat",
						--inline = true,
						order = 3,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Chat',
								desc = 'Click this to toggle viewing the main chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("SHOUT", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("SHOUT") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Global chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("SHOUT", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("SHOUT") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("SHOUT") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								values = Horizons.Config:GetChatFrameList(),
								width = "full",
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "SHOUT") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("SHOUT") end
							}
						}
					},
					tank = {
						type = "group",
						name = "Tank Chat",
						--inline = true,
						order = 4,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Tank Chat',
								desc = 'Click this to toggle viewing the tank chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("TANK", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("TANK") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Tank chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("TANK", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("TANK")end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("TANK") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "TANK") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("TANK") end
							}
						}
					},
					heal = {
						type = "group",
						name = "Heal Chat",
						--inline = true,
						order = 5,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Heal Chat',
								desc = 'Click this to toggle viewing the heal chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("HEAL", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("HEAL") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Heal chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("HEAL", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("HEAL")end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("HEAL") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "HEAL") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("HEAL") end
							}
						}
					},
					druid = {
						type = "group",
						name = "Druid Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Druid Chat',
								desc = 'Click this to toggle viewing the Druid chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("DRUID", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("DRUID") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Druid chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("DRUID", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("DRUID") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("DRUID") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "DRUID") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("DRUID") end
							}
						}
					},
					priest = {
						type = "group",
						name = "Priest Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Priest Chat',
								desc = 'Click this to toggle viewing the priest chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("PRIEST", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("PRIEST") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Priest chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("PRIEST", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("PRIEST") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("PRIEST") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "PRIEST") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("PRIEST") end
							}
						}
					},
					shaman = {
						type = "group",
						name = "Shaman Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Shaman Chat',
								desc = 'Click this to toggle viewing the shaman chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("SHAMAN", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("SHAMAN") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Shaman chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("SHAMAN", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("SHAMAN") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("SHAMAN") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "SHAMAN") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("SHAMAN") end
							}
						}
					},
					paladin = {
						type = "group",
						name = "Paladin Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Paladin Chat',
								desc = 'Click this to toggle viewing the paladin chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("PALADIN", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("PALADIN") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Paladin chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("PALADIN", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("PALADIN") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("PALADIN") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "PALADIN") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("PALADIN") end
							}
						}
					},
					warrior = {
						type = "group",
						name = "Warrior Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Warrior Chat',
								desc = 'Click this to toggle viewing the Warrior chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("WARRIOR", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("WARRIOR") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Warrior chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("WARRIOR", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("WARRIOR") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("WARRIOR") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "WARRIOR") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("WARRIOR") end
							}
						}
					},
					deathknight = {
						type = "group",
						name = "Death Knight Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Death Knight Chat',
								desc = 'Click this to toggle viewing the Death Knight chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("DEATHKNIGHT", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("DEATHKNIGHT") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Death Knight chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("DEATHKNIGHT", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("DEATHKNIGHT") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("DEATHKNIGHT") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "DEATHKNIGHT") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("DEATHKNIGHT") end
							}
						}
					},
					hunter = {
						type = "group",
						name = "Hunter Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Hunter Chat',
								desc = 'Click this to toggle viewing the Hunter chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("HUNTER", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("HUNTER") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Hunter chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("HUNTER", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("HUNTER") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("HUNTER") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "HUNTER") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("HUNTER") end
							}
						}
					},
					warlock = {
						type = "group",
						name = "Warlock Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Warlock Chat',
								desc = 'Click this to toggle viewing the Warlock chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("WARLOCK", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("WARLOCK") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Warlock chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("WARLOCK", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("WARLOCK") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("WARLOCK") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "WARLOCK") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("WARLOCK") end
							}
						}
					},
					mage = {
						type = "group",
						name = "Mage Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Mage Chat',
								desc = 'Click this to toggle viewing the Mage chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("MAGE", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("MAGE") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Mage chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("MAGE", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("MAGE") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("MAGE") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "MAGE") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("MAGE") end
							}
						}
					},
					rogue = {
						type = "group",
						name = "Rogue Chat",
						--inline = true,
						args = {
							toggle = {
								type = 'toggle',
								name = 'Enable Rogue Chat',
								desc = 'Click this to toggle viewing the Rogue chat',
								width = "full",
								set = function(info, val) Horizons.Config:ToggleChannelEnabled("ROGUE", val)  end,
								get = function() return Horizons.Config:IsChannelEnabled("ROGUE") end
							},
							color = {
								type = "color",
								name = "Pick Color",
								desc = "Pick a color for Rogue chat",
								set = function(info, r, g, b) Horizons.Config:SetChannelColor("ROGUE", r, g, b) end,
								get = function() return Horizons.Config:GetChannelColor("ROGUE") end
							},
							reset = {
								type = "execute",
								name = "Reset color",
								desc = "Reset color back to standard",
								func = function() Horizons.Config:ResetChannelColor("ROGUE") end
							},
							select = {
								type = "select",
								name = "Select Chat Frame",
								desc = "Select the chat frame to print this channel",
								width = "full",
								values = Horizons.Config:GetChatFrameList(),
								set = function(info, key) Horizons.Config:SetCurrentChatFrameForChat(key, "ROGUE") end,
								get = function(info, value) return Horizons.Config:GetCurrentChatFrameForChat("ROGUE") end
							}
						}
					}
				}
			},
			partyraid = {
				type = "group",
				name = "Party & Raid Config",
				args = {
					header = {
						type = "header",
						name = "Party & Raid Config",
						order = 1,
					},
					general = {
						type = "group",
						name = "General Settings",
						inline = true,
						order = 2,
						args = {
							allowtakeleader = {
								type = "toggle",
								name = "Everyone can take Party",
								desc = "Click this to toggle",
								width = "double",
								set = function(info, val) Horizons.db.global.Settings.Group.TakePartyAllow = val end,
								get = function() return Horizons.db.global.Settings.Group.TakePartyAllow end
							},
							allowtakealt = {
								type = "toggle",
								name = "Everyone can take Assist",
								desc = "Click this to toggle",
								width = "double",
								set = function(info, val) Horizons.db.global.Settings.Group.TakeAltAllow = val end,
								get = function() return Horizons.db.global.Settings.Group.TakeAltAllow end
							}
						}
					}
				}
			},
			general = {
				type = "group",
				name = "General Settings",
				order = 1,
				args = {
					header = {
						type = "header",
						name = "General Settings",
						order = 1,
					},
					description = {
						type = "description",
						name = function() return Horizons.Tools:FormatText("This is your currently selected main: _ROGUE_"..Horizons.db.global.Settings.PlayerInfo.Characters[Horizons.db.global.Settings.PlayerInfo.Main]) end,
						order = 1,
					},
					select = {
						type = "select",
						name = "Select Main Character",
						desc = "Select your main character here",
						order = 2,
						values = Horizons.Config:GetCharacterList(),
						set = function(info, key) Horizons.Config:SetCharacterMain(key) end,
						get = function() return Horizons.Config:GetCharacterMain() end
					},
					toggle = {
						type = 'toggle',
						name = 'Play Sound Files',
						desc = 'Click this to toggle',
						width = "full",
						set = function(info, val) Horizons.db.global.Settings.General.PlaySoundFiles = val  end,
						get = function() return Horizons.db.global.Settings.General.PlaySoundFiles end
					}
				}
			},
			modules = {
				type = "group",
				name = "Modules",
				args = {
				
				}
			}
		}
	}
	
	for _, v in pairs(Horizons.Modules.ModuleList) do
		local temp = {
				type = "group",
				name = v.Name,
				args = {
					toggle = {
						type = 'toggle',
						name = 'Enable',
						desc = 'Click this to toggle this module.',
						width = "full",
						order = 1,
						set = function() Horizons.Modules:ToggleModule(v.Name) end,
						get = function() return Horizons.Modules:GetModuleState(v.Name) end
					}					
				}
			}
		if v.DefaultOptions then
			temp.args.options = {
						type = "group",
						name = "Additional Settings",
						inline = true,
						order = 2,
						args = v.DefaultOptions,
						disabled = function() return not Horizons.Modules:GetModuleState(v.Name) end
					}
		
		end
		options.args.modules.args[temp.name] = temp
	end
	
	LibStub("AceConfig-3.0"):RegisterOptionsTable("Horizons", options)
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Horizons", "Horizons")
end

function Horizons.Config:GetDefaultDB()
	local defaults = {
		global = {
			Settings = {
				General = {
					PlaySoundFiles = true,
				},
				Channel = {
					Locked = true,
					HyperlinkUrls = true,
					['**'] = {
						Visible = false,
						ChatFrame = 1
					},
					SHOUT = {
						Visible = true
					}
				},
				Group = {
					TakePartyAllow = false,
					TakeAltAllow = true
				},
				PlayerInfo = {
					Main = nil,
					Characters = {}				
				},
				Modules = {
					['**'] = {
						Enabled = false
					}
				}
			}
		}
	}	
	return defaults
end

function Horizons.Config:InitialiseDB()
	Horizons.db = LibStub("AceDB-3.0"):New("HorizonsDB", Horizons.Config:GetDefaultDB(), true)
	Horizons.db.global.Settings.Channel[Horizons:GetPlayerClass("player")].Visible = true	
	
	--update playerinfo
	local name = UnitName("player")
	local isNotNew = true
	for i, v in pairs(Horizons.db.global.Settings.PlayerInfo.Characters) do
		if(v == name) then isNotNew = false end
	end
	if(isNotNew) then tinsert(Horizons.db.global.Settings.PlayerInfo.Characters, name) end	
	if(Horizons.db.global.Settings.PlayerInfo.Main == nil) then 
		Horizons.db.global.Settings.PlayerInfo.Main = 1
	end
end

function Horizons.Config:IsChannelEnabled(channel)
	if (Horizons.db.global.Settings.Channel[channel].Visible) then
		return (Horizons.db.global.Settings.Channel[channel].Visible)
	else
		return false
	end
end

function Horizons.Config:ToggleChannelEnabled(channel, val)
	if (Horizons.db.global.Settings.Channel[channel]) then
		Horizons.db.global.Settings.Channel[channel].Visible = val
	else
		Horizons.db.global.Settings.Channel[channel] = {Visible = val}
	end
end

function Horizons.Config:GetChannelColor(channel, sender)
	local r = 1
	local g = 1
	local b = 1
	
	if(sender == nil) then
		sender = ""
	end
	
	if(Horizons.db.global.Settings.Channel[string.upper(channel)].Color) then
		r = Horizons.db.global.Settings.Channel[string.upper(channel)].Color.r
		g = Horizons.db.global.Settings.Channel[string.upper(channel)].Color.g
		b = Horizons.db.global.Settings.Channel[string.upper(channel)].Color.b
	else
		r, g, b  = Horizons.Config:GetStandardColor(channel)
	end
	if Horizons.Config:IsOfficer( sender ) then
        r = r * 0.80
        g = g * 0.80
        b = b * 0.80
    end
	return r, g, b
end

function Horizons.Config:GetStandardColor(channel)
		local color = { r=1, g=1, b=1}
		if channel == "SHOUT" then
			color = { r=0.7, g=0.7, b=1.0 }
		elseif channel == "HEAL" then
			color = { r=0.7, g=0.2, b=0.2 }
		elseif channel == "TANK" then
			color = { r=0.78, g=0.61, b=0.43 }
		elseif channel == "DEATHKNIGHT" and type(RAID_CLASS_COLORS) == "table" and type(RAID_CLASS_COLORS[channel]) == "table" then
			color = RAID_CLASS_COLORS["DEATHKNIGHT"]
		elseif type(RAID_CLASS_COLORS) == "table" and type(RAID_CLASS_COLORS[channel]) == "table" then 
			color = RAID_CLASS_COLORS[channel]
		end
		return color.r, color.g, color.b
end

function Horizons.Config:SetChannelColor(channel, red, green, blue)	
	if(Horizons.db.global.Settings.Channel[channel].Color) then
		Horizons.db.global.Settings.Channel[channel].Color.r = red
		Horizons.db.global.Settings.Channel[channel].Color.g = green
		Horizons.db.global.Settings.Channel[channel].Color.b = blue
	else
		Horizons.db.global.Settings.Channel[channel].Color = {r = red, g = green, b = blue}
	end
end

function Horizons.Config:ResetChannelColor(channel)
	Horizons.Config:SetChannelColor(channel, Horizons.Config:GetStandardColor(channel))
end

function Horizons.Config:GetChannelChatFrame(channel)	
	return getglobal( "ChatFrame"..Horizons.db.global.Settings.Channel[channel].ChatFrame )
end

function Horizons.Config:GetChatFrameList()
	local numFrames = NUM_CHAT_WINDOWS
    local list = {}
    for i = 1, numFrames, 1 do
		local name, fontSize, r, g, b, alpha, shown, locked = GetChatWindowInfo(i)
        if name == "" then name = "ChatFrame" end
		if(not shown) then name = name .. " (Hidden)" end
		list[i] = i..". "..name
    end
	return list
end

function Horizons.Config:SetCurrentChatFrameForChat(key, channel)
	Horizons.db.global.Settings.Channel[channel].ChatFrame = key
end

function Horizons.Config:GetCurrentChatFrameForChat(channel)
	return Horizons.db.global.Settings.Channel[channel].ChatFrame
end

function Horizons.Config:GetCharacterList()
	local list = {}
	for i, v in pairs(Horizons.db.global.Settings.PlayerInfo.Characters) do
		list[i] = v
	end
	return list
end

function Horizons.Config:SetCharacterMain(key)
	Horizons.db.global.Settings.PlayerInfo.Main = key
end

function Horizons.Config:GetCharacterMain()
	return Horizons.db.global.Settings.PlayerInfo.Main
end

function Horizons.Config:GetCharacterMainName()
	return Horizons.db.global.Settings.PlayerInfo.Characters[Horizons.db.global.Settings.PlayerInfo.Main]
end

function Horizons.Config:AddOfficer(name)
	if(type(name) == "string") then
		if(not Horizons.Config.OfficerList) then
			Horizons.Config.OfficerList = {}
		end
		Horizons.Config.OfficerList[name] = true
	end
end

function Horizons.Config:IsOfficer(name)
	if(Horizons.Config.OfficerList) then
		return Horizons.Config.OfficerList[name]
	else
		return false
	end
end
