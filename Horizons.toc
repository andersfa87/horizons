## Interface: 50100
## Title: Horizons Guild Addon
## Notes: Horizons Guild Community Addon
## Author: Taziator
## Version: 0.899
## SavedVariables: HorizonsDB


embeds.xml

Core.lua
WowApi.lua
Util.lua
Comms.lua
Config.lua
Group.lua
Tools.lua
Tools\Alert.lua
Tools\Auction.lua
Tools\ProfessionCheck.lua
Tools\Locate.lua
Tools\IconAssignment.lua
Tools\DurabilityCheck.lua

Officer.lua

Modules.lua
Modules\Timestamps.lua
Modules\Autoaccept.lua
Modules\InterruptAnnounce.lua
Modules\AchievementAnnounce.lua
Modules\MovePowerBarAlt.lua
Modules\KeywordInvite.lua
Modules\RaidRoll.lua
