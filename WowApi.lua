Horizons.WowApi = {}


function Horizons.WowApi.IsInRaid(player)
	if(player ~= nil) then
		return UnitInRaid(player)
	end
	return UnitInRaid("player")
end

function Horizons.WowApi.IsInParty(player)
	if(player ~= nil) then
		return UnitInParty(player)
	end
	return UnitInParty("player")
end

function Horizons.WowApi.IsGroupLeader()
	return UnitIsGroupLeader(UnitName("player"))
end

function Horizons.WowApi.IsGroupAssistant()
	return UnitIsGroupAssistant(UnitName("player"))
end

function Horizons.WowApi.GetNumGroupMembers()
	return GetNumGroupMembers()
end

function Horizons.WowApi.UnitIsInMyGuild(sender)
	return UnitIsInMyGuild(sender)
end