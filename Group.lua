Horizons.Group = {}

function Horizons.Group.TakeParty()
	if (Horizons.WowApi.IsInRaid()) then
		if (not Horizons.WowApi.IsGroupLeader()) then
			Horizons:SendCommMessage("Horizons", "TAKEPARTY", "RAID")
		else
			Horizons:Print("You are already the raid leader");
		end
	elseif (Horizons.WowApi.IsInParty()) then
		if (not Horizons.WowApi.IsGroupLeader()) then
			Horizons:SendCommMessage("Horizons", "TAKEPARTY", "PARTY")
		else
			Horizons:Print("You are already the party leader");
		end
	end
end

function Horizons.Group.HandleTakeParty(input, sender)
	if (Horizons.WowApi.IsInRaid() and Horizons.WowApi.IsInRaid(sender) and Horizons.WowApi.IsGroupLeader()) then
		if (Horizons.db.global.Settings.Group.TakePartyAllow or Horizons.Config:IsOfficer(sender)) then
			PromoteToLeader(sender);
			Horizons:Print("Promoted " .. sender .. " to raid leader")
		else
			Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."Take Raid Denied", "WHISPER", sender)
		end
	elseif (Horizons.WowApi.IsInParty() and Horizons.WowApi.IsInParty(sender)) then
		if(Horizons.WowApi.IsGroupLeader()) then
			if (Horizons.db.global.Settings.Group.TakePartyAllow or Horizons.Config:IsOfficer(sender)) then
				PromoteToLeader(sender);
				Horizons:Print("Promoted " .. sender .. " to party leader")
			else
				Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."Take Party Denied", "WHISPER", sender)
			end
		end
	end
end

function Horizons.Group.TakeAlt()
	if (Horizons.WowApi.IsInRaid()) then
		Horizons:SendCommMessage("Horizons", "TAKEALT", "RAID")
	else 
		Horizons:Print("You are not in a raid group");
	end
end

function Horizons.Group.HandleTakeAlt(input, sender)
	if (Horizons.WowApi.IsGroupLeader() and Horizons.WowApi.IsInRaid(sender)) then
		if (Horizons.db.global.Settings.Group.TakeAltAllow or Horizons.Config:IsOfficer(sender)) then
			PromoteToAssistant(sender);
			Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."Take Assist Accepted", "WHISPER", sender)
			Horizons:Print("Promoted " .. sender .. " to raid assistant")
		else
			Horizons:SendCommMessage("Horizons", "MESSAGE"..Horizons.Comms.Separator.."Take Assist Denied", "WHISPER", sender)
		end
	end
end
